﻿using System;
using System.Diagnostics.CodeAnalysis;
using Evoshop.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SessionServiceTest
    {
        [TestMethod]
        public void Login_ExpectedUsage_Valid()
        {
            // Arrange
            var sessionService = new SessionService();
            var id = 1;
            // Act
            var guid = sessionService.Login(id);
            // Assert
            Assert.IsNotNull(guid);
            Assert.IsTrue(sessionService.IsLoggedIn(guid));
            Assert.IsTrue(sessionService.IsLoggedIn(id));
            Assert.AreEqual(guid, sessionService.GetGuid(id));
            Assert.AreEqual(id, sessionService.GetUser(guid));
        }

        [TestMethod]
        public void Logout_ExpectedUsage_Valid()
        {
            // Arrange
            var sessionService = new SessionService();
            var id = 1;
            // Act
            var guid = sessionService.Login(id);
            var isLogoutSuccessed = sessionService.Logout(guid);
            // Assert
            Assert.IsNotNull(guid);
            Assert.IsNotNull(isLogoutSuccessed);
            Assert.IsTrue(isLogoutSuccessed);
            Assert.IsFalse(sessionService.IsLoggedIn(guid));
            Assert.IsFalse(sessionService.IsLoggedIn(id));
        }

        [TestMethod]
        public void Logout_NoLogin_Throws()
        {
            // Arrange
            var sessionService = new SessionService();
            var guid = new Guid();
            // Act
            var isLogoutSuccessed = sessionService.Logout(guid);
            // Assert
            Assert.IsNotNull(isLogoutSuccessed);
            Assert.IsFalse(isLogoutSuccessed);
            Assert.IsFalse(sessionService.IsLoggedIn(guid));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "The user can not be found in the session repository.")]
        public void GetGuid_NoLogin_Throws()
        {
            // Arrange
            var sessionService = new SessionService();
            var id = 1;
            // Act
            sessionService.GetGuid(id);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "The GUID can not be found in the session repository.")]
        public void GetUser_NoLogin_Throws()
        {
            // Arrange
            var sessionService = new SessionService();
            var guid = new Guid();
            // Act
            sessionService.GetUser(guid);
        }
    }
}
