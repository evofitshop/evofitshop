﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using Evoshop.Models;
using Evoshop.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class UserServiceTest
    {
        private ISessionService _sessionService = Substitute.For<ISessionService>();
        private IUserContext _db = new InMemorySqlite().UserContext;

        private IUserService _userService = new UserService();

        [TestInitialize]
        public void TestInitialize()
        {
            CompositionHelper.CompositionContainer = new CompositionContainer();
            CompositionHelper.CompositionContainer.ComposeExportedValue(_sessionService);
            CompositionHelper.CompositionContainer.ComposeExportedValue(_db);

            CompositionHelper.CompositionContainer.SatisfyImportsOnce(_userService);
        }

        [TestMethod]
        public void InMemorySqLite_Works_Valid()
        {
            Assert.IsTrue(_db.Database.CanConnect());
        }

        [TestMethod]
        public void Register_ExpectedUsage_Valid()
        {
            // Arrange
            var expectedGuid = new Guid();
            _sessionService.Login(Arg.Any<int>()).Returns(expectedGuid);

            var emailAddress = "aaa@aaa.com";
            var password = "123456Aa";
            var name = "a";
            var phoneNumber = "+36301231234";
            var addressDto = new AddressDto() { ZipCode = "1000", City = "a", Street = "a", HouseNumber = "1" };
            // Act
            var actualGuid = _userService.CreateUser(emailAddress,password,name,phoneNumber,addressDto);
            // Assert
            Assert.IsNotNull(actualGuid);
            Assert.IsTrue(_db.Users.Count() == 1);
            Assert.AreEqual(expectedGuid, actualGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Email address is already taken.")]
        public void Register_EmailAlreadyExists_Throws()
        {
            // Arrange
            var emailAddress = "aaa@aaa.com";
            var password = "123456Aa";
            var name = "a";
            var phoneNumber = "+36301231234";
            var addressDto = new AddressDto() { ZipCode = "1000", City = "a", Street = "a", HouseNumber = "1" };

            var newUser = new User(emailAddress, password, name, phoneNumber, new Address(addressDto));
            _db.Users.Add(newUser);
            _db.SaveChanges();
            // Act
            _userService.CreateUser(emailAddress, password, name, phoneNumber, addressDto);
        }

        [TestMethod]
        public void GetUser_ExpectedUsage_Valid()
        {
            // Arrange
            var guid = new Guid();

            var emailAddress = "aaa@aaa.com";
            var password = "123456Aa";
            var name = "a";
            var phoneNumber = "+36301231234";
            var addressDto = new AddressDto() { ZipCode = "1000", City = "a", Street = "a", HouseNumber = "1" };
            var address = new Address(addressDto);

            var expectedUser = new User(emailAddress, password, name, phoneNumber, address);
            _db.Users.Add(expectedUser);
            _db.SaveChanges();

            _sessionService.GetUser(Arg.Any<Guid>()).Returns(expectedUser.Id);
            // Act
            var actualUser = _userService.GetUser(guid);
            // Assert
            Assert.IsNotNull(actualUser);
            Assert.AreEqual(expectedUser.Id, actualUser.Id);
            Assert.AreEqual(expectedUser.EmailAddress, actualUser.EmailAddress);
            Assert.AreEqual(expectedUser.Name, actualUser.Name);
            Assert.AreEqual(expectedUser.PhoneNumber, actualUser.PhoneNumber);
            Assert.IsNotNull(actualUser.Addresses);
            Assert.IsNotNull(actualUser.Addresses.First());
            Assert.AreEqual(expectedUser.Addresses.First(), actualUser.Addresses.First());
        }

        [TestMethod]
        public void Login_ExpectedUsage_Valid()
        {
            // Arrange
            var expectedGuid = new Guid();

            var emailAddress = "aaa@aaa.com";
            var password = "123456Aa";
            var name = "a";
            var phoneNumber = "+36301231234";
            var addressDto = new AddressDto() { ZipCode = "1000", City = "a", Street = "a", HouseNumber = "1" };
            var address = new Address(addressDto);

            var user = new User(emailAddress, password, name, phoneNumber, address);
            _db.Users.Add(user);
            _db.SaveChanges();

            _sessionService.Login(Arg.Any<int>()).Returns(expectedGuid);
            // Act
            var actualGuid = _userService.Login(emailAddress, password);
            // Assert
            Assert.IsNotNull(actualGuid);
            Assert.AreEqual(expectedGuid, actualGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Incorrect username or password.")]
        public void Login_Incorrect_Throws()
        {
            // Arrange
            var emailAddress = "aaa@aaa.com";
            var password = "123456Aa";
            // Act
            _userService.Login(emailAddress, password);
        }

        [TestMethod]
        public void Logout_ExpectedUsageTrue_Valid()
        {
            // Arrange
            var guid = new Guid();
            _sessionService.Logout(guid).Returns(true);
            // Act
            var result = _userService.Logout(guid);
            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Logout_ExpectedUsageFalse_Valid()
        {
            // Arrange
            var guid = new Guid();
            _sessionService.Logout(guid).Returns(false);
            // Act
            var result = _userService.Logout(guid);
            // Assert
            Assert.IsNotNull(result);
            Assert.IsFalse(result);
        }
    }
}
