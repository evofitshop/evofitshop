﻿using Evoshop.DataAccess;
using Evoshop.Interfaces;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    public class InMemorySqlite
    {
        private const string InMemoryConnectionString = "DataSource=:memory:";
        private SqliteConnection _connection;

        public IUserContext UserContext;

        public InMemorySqlite()
        {
            _connection = new SqliteConnection(InMemoryConnectionString);
            _connection.Open();
            var options = new DbContextOptionsBuilder<UserContext>()
                    .UseSqlite(_connection)
                    .Options;
            UserContext = new UserContext(options);
            UserContext.Database.EnsureCreated();
        }
    }
}
