﻿using Evoshop.Controllers;
using Evoshop.Dtos;
using Evoshop.Interfaces;
using Evoshop.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SessionControllerTest
    {
        private IUserService _userService = Substitute.For<IUserService>();

        [TestInitialize]
        public void TestInitialize()
        {
            CompositionHelper.CompositionContainer = new CompositionContainer();
            CompositionHelper.CompositionContainer.ComposeExportedValue(_userService);
        }

        #region Login

        [TestMethod]
        public void Login_DtoNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            LoginUserDto dto = null;
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Login_EmailNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            var dto = new LoginUserDto()
            {
                EmailAddress = null,
                Password = "password"
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Login_ExpectedUsage_Valid()
        {
            // Arrange
            var expectedGuid = "d7ee76d3-2643-406f-a3d9-772a7724d249";
            _userService.Login(Arg.Any<string>(), Arg.Any<string>()).Returns(Guid.Parse(expectedGuid));

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            var dto = new LoginUserDto()
            {
                EmailAddress = "email",
                Password = "password"
            };

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.OK, actualResponse.StatusCode);
            var actualGuid = actualResponse.Content.ReadAsAsync<string>().Result.Trim('"');
            Assert.AreEqual(expectedGuid, actualGuid);
        }

        [TestMethod]
        public void Login_UserServiceException_Throws()
        {
            // Arrange
            var expectedMessage = "exception message from UserService";
            _userService.Login(Arg.Any<string>(), Arg.Any<string>()).Throws(new Exception(expectedMessage));

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            var dto = new LoginUserDto()
            {
                EmailAddress = "email",
                Password = "password"
            };

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.InternalServerError, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsAsync<string>().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        #endregion Login

        #region Register

        [TestMethod]
        public void Register_ExpectedUsage_Valid()
        {
            // Arrange
            var expectedGuid = "d7ee76d3-2643-406f-a3d9-772a7724d249";
            _userService.CreateUser(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<AddressDto>()).Returns(Guid.Parse(expectedGuid));

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.OK, actualResponse.StatusCode);
            var actualGuid = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedGuid, actualGuid);
        }

        [TestMethod]
        public void Register_UserServiceException_Throws()
        {
            // Arrange
            var expectedMessage = "exception message from UserService";
            _userService.CreateUser(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<AddressDto>()).Throws(new Exception(expectedMessage));

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.InternalServerError, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_DtoNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = null;
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_EmailNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = null,
                Password = "0",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_AddressNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "0",
                Name = "name",
                PhoneNumber = "0",
                Address = null
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_PasswordNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = null,
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_NameNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = null,
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_PhoneNumberNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = null,
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_ZipCodeNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = null,
                    City = "0",
                    Street = "0",
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_CityNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = null,
                    Street = "0",
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_StreetNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = null,
                    HouseNumber = "a"
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Register_HouseNumberNull_Throws()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            RegisterUserDto dto = new RegisterUserDto()
            {
                EmailAddress = "a",
                Password = "a",
                Name = "name",
                PhoneNumber = "0",
                Address = new AddressDto()
                {
                    ZipCode = "0",
                    City = "0",
                    Street = "0",
                    HouseNumber = null
                }
            };
            var expectedMessage = "The body DTO needs to be filled out.";

            // Act
            var actualResponse = sessionController.Post(dto);

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        #endregion Register

        #region Logout

        [TestMethod]
        public void Logout_GuidHeaderDoesntExist_Throws()
        {
            // Arrange
            var expectedMessage = "The GUID is invalid.";

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            //sessionController.Request.Headers.Add("guid", "invalid value");

            // Act
            var actualResponse = sessionController.Post();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.Unauthorized, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Logout_GuidInvalid_Throws()
        {
            // Arrange
            var expectedMessage = "The GUID is invalid.";

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            sessionController.Request.Headers.Add("guid", "invalid value");

            // Act
            var actualResponse = sessionController.Post();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.Unauthorized, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Logout_GuidEmpty_Throws()
        {
            // Arrange
            var expectedMessage = "The GUID is invalid.";

            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            sessionController.Request.Headers.Add("guid", string.Empty);

            // Act
            var actualResponse = sessionController.Post();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.Unauthorized, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Logout_ExpectedUsage_Valid()
        {
            // Arrange
            var sessionController = new SessionController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            sessionController.Request.Headers.Add("guid", "d7ee76d3-2643-406f-a3d9-772a7724d249");

            // Act
            var actualResponse = sessionController.Post();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.OK, actualResponse.StatusCode);
        }

        #endregion Logout
    }
}
