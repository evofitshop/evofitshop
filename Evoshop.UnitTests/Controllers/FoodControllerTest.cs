﻿using Evoshop.Controllers;
using Evoshop.Interfaces;
using Evoshop.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Type = Evoshop.Controllers.Type;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class FoodControllerTest
    {
        private IUserContext _db = new InMemorySqlite().UserContext;

        [TestInitialize]
        public void TestInitialize()
        {
            CompositionHelper.CompositionContainer = new CompositionContainer();
            CompositionHelper.CompositionContainer.ComposeExportedValue(_db);
        }

        [TestMethod]
        public void GetFoodData_ExpectedUsage_Valid()
        {
            // Arrange
            List<Product> products = new List<Product>
            {
                new Food(Type.koret, "Sültkrumpli", "Sültkrupmli", 290, foodType.vega, DateTime.Parse("2020/05/18")),
                new Pizza(890, 1650, Type.pizza, "Hawaii Pizza", "paradicsomos alap, sonka, sajt, ananász", 1190, foodType.husos, DateTime.Parse("2020/05/27"))
            };
            _db.Products.AddRange(products);
            _db.SaveChanges();

            var foodController = new FoodController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            // Act
            var actualResponse = foodController.getFoodData();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.OK, actualResponse.StatusCode);
            var actualList = actualResponse.Content.ReadAsAsync<List<Product>>().Result;
            Assert.AreEqual(products.Count, actualList.Count);
            Assert.AreEqual(products[0], actualList[0]);
            Assert.AreEqual(products[1], actualList[1]);
        }
    }
}
