﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using Evoshop.Controllers;
using Evoshop.Interfaces;
using Evoshop.Models;
using Evoshop.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class UserControllerTest
    {
        private IUserService _userService = Substitute.For<IUserService>();

        [TestInitialize]
        public void TestInitialize()
        {
            CompositionHelper.CompositionContainer = new CompositionContainer();
            CompositionHelper.CompositionContainer.ComposeExportedValue(_userService);
        }

        [TestMethod]
        public void Get_GuidHeaderDoesntExist_Throws()
        {
            // Arrange
            var expectedMessage = "The GUID is invalid.";

            var userController = new UserController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };

            // Act
            var actualResponse = userController.Get();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.Unauthorized, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Get_GuidHeaderIsEmpty_Throws()
        {
            // Arrange
            var expectedMessage = "The GUID is invalid.";

            var userController = new UserController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            userController.Request.Headers.Add("guid", string.Empty);

            // Act
            var actualResponse = userController.Get();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.Unauthorized, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Get_GuidHeaderIsInvalid_Throws()
        {
            // Arrange
            var expectedMessage = "The GUID is invalid.";

            var userController = new UserController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            userController.Request.Headers.Add("guid", "invalid value");

            // Act
            var actualResponse = userController.Get();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.Unauthorized, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Get_UserServiceException_Throws()
        {
            // Arrange
            var expectedMessage = "exception from UserService";
            _userService.GetUser(Arg.Any<Guid>()).Throws(new Exception(expectedMessage));

            var userController = new UserController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            userController.Request.Headers.Add("guid", "d7ee76d3-2643-406f-a3d9-772a7724d249");

            // Act
            var actualResponse = userController.Get();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
            var actualMessage = actualResponse.Content.ReadAsStringAsync().Result.Trim('"');
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void Get_ExpectedUsage_Valid()
        {
            // Arrange
            var expectedUser = new User();
            _userService.GetUser(Arg.Any<Guid>()).Returns(expectedUser);

            var userController = new UserController
            {
                Configuration = new HttpConfiguration(),
                Request = new HttpRequestMessage()
            };
            userController.Request.Headers.Add("guid", "d7ee76d3-2643-406f-a3d9-772a7724d249");

            // Act
            var actualResponse = userController.Get();

            // Assert
            Assert.IsNotNull(actualResponse);
            Assert.AreEqual(HttpStatusCode.OK, actualResponse.StatusCode);
            var actualUser = actualResponse.Content.ReadAsAsync<User>().Result;
            Assert.IsNotNull(actualUser);
            Assert.IsInstanceOfType(actualUser, typeof(User));
            Assert.AreEqual(expectedUser, actualUser);
        }
    }
}
