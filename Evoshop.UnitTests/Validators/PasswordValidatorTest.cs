﻿using System;
using System.Diagnostics.CodeAnalysis;
using Evoshop.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class PasswordValidatorTest
    {
        [TestMethod]
        public void Validate_ExpectedUsage_Valid()
        {
            // Arrange
            var passwords = new string[]
            {
                "123456aA",
                "AAAAAAA1234aa",
                "Ab1234",
                "Abcde1",
                "aBCDE2",
                "24T260DiGv2C6%m8QNLctU&6#LY^@!lxZP@"
            };

            // Act
            foreach (var email in passwords)
            {
                PasswordValidator.Validate(email);
            }
        }

        [TestMethod]
        public void Validate_InvalidPasswords_Throws()
        {
            // Arrange
            var passwords = new string[]
            {
                "123456",
                "abcdef",
                "ABCDEF",
                "A12BDFBDFGBFD6756756",
                "a234cvdf234fxv",
                "ADFGDF43CVBDF",
                "123[[Đđ]Đ@]{Ä{Ä^|˘°{|˘^"
            };

            int errorCount = 0;

            // Act
            foreach (var password in passwords)
            {
                try
                {
                    PasswordValidator.Validate(password);
                }
                catch (ArgumentException)
                {
                    errorCount++;
                }
            }

            Assert.IsTrue(errorCount == passwords.Length, $"Error count is {errorCount} instead of {passwords.Length}");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueNull_Throws()
        {
            // Arrange
            string value = null;

            // Act
            PasswordValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueWhitespace_Throws()
        {
            // Arrange
            string value = " ";

            // Act
            PasswordValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueEmpty_Throws()
        {
            // Arrange
            string value = string.Empty;

            // Act
            PasswordValidator.Validate(value);
        }
    }
}
