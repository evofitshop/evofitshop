﻿using System;
using System.Diagnostics.CodeAnalysis;
using Evoshop.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class PhoneNumberValidatorTest
    {
        [TestMethod]
        public void Validate_ExpectedUsage_Valid()
        {
            // Arrange
            var phoneNumbers = new string[]
            {
                "+36301231234",
                "+36705554444",
                "+36207878121"
            };

            // Act
            foreach (var phoneNumber in phoneNumbers)
            {
                PhoneNumberValidator.Validate(phoneNumber);
            }
        }

        [TestMethod]
        public void Validate_Invalid_Throws()
        {
            // Arrange
            var phoneNumbers = new string[]
            {
                "+36120231034",
                "06301231234",
                "+1 321 6547 6547",
                "07 654 12554 12",
                "invalid",
            };

            int errorCount = 0;

            // Act
            foreach (var phoneNumber in phoneNumbers)
            {
                try
                {
                    PhoneNumberValidator.Validate(phoneNumber);
                }
                catch (ArgumentException)
                {
                    errorCount++;
                }
            }

            Assert.IsTrue(errorCount == phoneNumbers.Length, $"Error count is {errorCount} instead of {phoneNumbers.Length}");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueNull_Throws()
        {
            // Arrange
            string value = null;

            // Act
            PhoneNumberValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueWhitespace_Throws()
        {
            // Arrange
            string value = " ";

            // Act
            PhoneNumberValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueEmpty_Throws()
        {
            // Arrange
            string value = string.Empty;

            // Act
            PhoneNumberValidator.Validate(value);
        }
    }
}
