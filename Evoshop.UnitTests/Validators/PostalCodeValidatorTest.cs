﻿using System;
using System.Diagnostics.CodeAnalysis;
using Evoshop.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class PostalCodeValidatorTest
    {
        [TestMethod]
        public void Validate_ExpectedUsage_Valid()
        {
            // Arrange
            var postalCodes = new string[]
            {
                "1000",
                "3515",
                "9999"
            };

            // Act
            foreach (var postalCode in postalCodes)
            {
                PostalCodeValidator.Validate(postalCode);
            }
        }

        [TestMethod]
        public void Validate_Invalid_Throws()
        {
            // Arrange
            var postalCodes = new string[]
            {
                "999",
                "10000",
                "1",
                "4564564564",
                "something",
                "1O1O",
                "MMMDXIII",
                "110110111011"
            };

            int errorCount = 0;

            // Act
            foreach (var postalCode in postalCodes)
            {
                try
                {
                    PostalCodeValidator.Validate(postalCode);
                }
                catch (ArgumentException)
                {
                    errorCount++;
                }
            }

            Assert.IsTrue(errorCount == postalCodes.Length, $"Error count is {errorCount} instead of {postalCodes.Length}");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueNull_Throws()
        {
            // Arrange
            string value = null;

            // Act
            PostalCodeValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueWhitespace_Throws()
        {
            // Arrange
            string value = " ";

            // Act
            PostalCodeValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validate_ValueEmpty_Throws()
        {
            // Arrange
            string value = string.Empty;

            // Act
            PostalCodeValidator.Validate(value);
        }
    }
}
