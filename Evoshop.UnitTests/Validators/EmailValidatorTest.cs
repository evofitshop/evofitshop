﻿using System;
using System.Diagnostics.CodeAnalysis;
using Evoshop.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoshop.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class EmailValidatorTest
    {
        [TestMethod]
        public void Validate_ExpectedUsage_Valid()
        {
            // Arrange
            var emailAddresses = new string[]
            {
                "user1112@example.hu",
                "user.user2343@sub.domain.what",
                "u.s.e.r.1.2.3.4.6@gmail.com",
                "valami@a.a.a.a.a.a.a.a.a.a.a.com",
                "      a123@a123.ba          ",
                "valami.valami+evoshop@gmail.com"
            };

            // Act
            foreach (var email in emailAddresses)
            {
                EmailValidator.Validate(email);
            }
        }

        [TestMethod]
        public void Validate_InvalidEmails_Throws()
        {
            // Arrange
            var emailAddresses = new string[]
            {
                "us     er1112@example.hu",
                "val*ami@a.a.a.a*.a.a.*a.a.a.a.*a.com",
                "      a123@a1 23.ba          ",
                "val ami.valami@evoshop@gmail.com",
                "@",
                "asd@",
                "asd&@Đ[].com"
            };

            int errorCount = 0;

            // Act
            foreach (var email in emailAddresses)
            {
                try
                {
                    EmailValidator.Validate(email);
                }
                catch (ArgumentException)
                {
                    errorCount++;
                }
            }

            Assert.IsTrue(errorCount == emailAddresses.Length, $"Error count is {errorCount} instead of {emailAddresses.Length}");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "The email address can not be null or empty.")]
        public void Validate_ValueNull_Throws()
        {
            // Arrange
            string value = null;

            // Act
            EmailValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "The email address can not be null or empty.")]
        public void Validate_ValueWhitespace_Throws()
        {
            // Arrange
            string value = " ";

            // Act
            EmailValidator.Validate(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "The email address can not be null or empty.")]
        public void Validate_ValueEmpty_Throws()
        {
            // Arrange
            string value = string.Empty;

            // Act
            EmailValidator.Validate(value);
        }
    }
}
