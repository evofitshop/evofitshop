﻿using Evoshop.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.Composition;
using System;
using Evoshop.Controllers;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Evoshop.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.DataAccess
{
    [ExcludeFromCodeCoverage]
    [Export(typeof(IUserContext)), PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserContext : DbContext, IUserContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Cart> GuestCarts { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }

        public DbSet<CalculateCalorie> Caloriedata { get; set; }

        // public DbSet<Drink> Drinks { get; set; }

        public UserContext(DbContextOptions<UserContext> options) : base(options) { }

        public UserContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                const string Server = "34.89.179.252";
                const string SqlDatabase = "evofitshop";
                const string User = "root";
                const string Password = "1qU9W7JeSVPz2UYt55Ld9WZxel646f3u";

                optionsBuilder.UseMySql($"Server={Server};Database={SqlDatabase};User={User};Password={Password};", mySqlOptions => mySqlOptions.ServerVersion(new Version(8, 0, 22), ServerType.MySql));
            }
        }
    }
}