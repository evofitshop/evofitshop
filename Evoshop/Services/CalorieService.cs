﻿using Evoshop.Interfaces;
using Evoshop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Evoshop.Services
{
    [Export(typeof(ICalorieService))]
    public class CalorieService : ICalorieService
    {
        [Import]
        private IUserContext db;

        [Import]
        private ISessionService sessionService;

        public CalculateCalorie GetUser(Guid guid)
        {
            var userId = sessionService.GetUser(guid);
            var userName = db.Users.FirstOrDefault(u => u.Id == userId).Name;
            return db.Caloriedata.Where(u => u.Name == userName).FirstOrDefault();
        }


        /*public CalorieService()
        {
            CompositionHelper.CompositionContainer.SatisfyImportsOnce(this);
        }*/
        public int CreateCalorie(CalculateCalorie calorie)
        {
            calorie.CalculatedCalorie = (float)(calorie.Gender * calorie.Weight * 24 * calorie.Activity + calorie.Goal);
            bool find = db.Caloriedata.Any(s => s.Name.Contains(calorie.Name));
            var dw = calorie.DreamWeight;
            if (find)
            {
                CalculateCalorie v = db.Caloriedata.First(s => s.Name.Contains(calorie.Name));
                db.Caloriedata.Remove(v);
            }
            db.Caloriedata.Add(calorie);
            db.SaveChanges();

            return calorie.Id;
        }
    }
}