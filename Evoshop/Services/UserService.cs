﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using Evoshop.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Evoshop.Services
{
    [Export(typeof(IUserService))]
    public class UserService : IUserService
    {
        [Import]
        private IUserContext db;
        [Import]
        private ISessionService sessionService;

        public User GetUser(Guid guid)
        {
            var userId = sessionService.GetUser(guid);
            return db.Users.Where(u => u.Id == userId).Include(u => u.Addresses).Include(u => u.Ratings).Include(u => u.Cart).Include(u => u.Orders).FirstOrDefault();
        }

        public Guid CreateUser(string emailAddress, string password, string name, string phoneNumber, AddressDto addressDto)
        {
            var address = new Address(addressDto);

            var user = db.Users.Where(u => u.EmailAddress == emailAddress).FirstOrDefault();

            if (user == null)
            {
                var newUser = new User(emailAddress, password, name, phoneNumber, address);

                db.Users.Add(newUser);
                db.SaveChanges();

                return sessionService.Login(newUser.Id);
            }
            else
            {
                throw new Exception("Email address is already taken.");
            }
        }

        public Guid Login(string emailAddress, string password)
        {
            var user = db.Users.Where(u => u.EmailAddress == emailAddress && u.Password == password).Include(u => u.Addresses).Include(u => u.Orders).Include(u => u.Ratings).FirstOrDefault();

            if (user != null)
            {
                return sessionService.Login(user.Id);
            }
            else
            {
                throw new Exception("Incorrect username or password.");
            }
        }

        public User EditUser(Guid userSession, string emailAddress, string password, string name, string phoneNumber, AddressDto addressDto)
        {
            var userId = sessionService.GetUser(userSession);

            if (db.Users.Where(u => u.Id != userId && u.EmailAddress == emailAddress).Count() > 0)
            {
                throw new Exception("Email address is already taken.");
            }

            var user = db.Users
                .Include(o => o.Addresses)
                .Where(u => u.Id == userId)
                .FirstOrDefault();

            var address = new Address(addressDto);

            if (user != null)
            {
                user.EmailAddress = emailAddress;
                user.Password = password;
                user.Name = name;
                user.PhoneNumber = phoneNumber;
                user.Addresses[0] = address;

                db.SaveChanges();

                return user;
            }
            else
            {
                throw new Exception("User not found.");
            }
        }

        public bool Logout(Guid guid)
        {
            return sessionService.Logout(guid);
        }
    }
}