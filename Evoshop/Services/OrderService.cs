﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using Evoshop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace Evoshop.Services
{
    [Export(typeof(IOrderService))]
    public class OrderService : IOrderService
    {
        [Import]
        private IUserContext _db;
        [Import]
        private IUserService _userService;

        public Order PlaceOrder(OrderDto orderDto, Guid userGuid)
        {
            var user = _userService.GetUser(userGuid);

            var cart = orderDto.Cart.Select(x => new OrderItem(_db.Products.Find(x.ProductId), x.Size, x.Quantity)).ToList();
            if (_db.Addresses.Find(orderDto.AddressId) == null)
            {
                throw new ArgumentOutOfRangeException(nameof(orderDto.AddressId));
            }

            var order = new Order(cart, user.Id, orderDto.AddressId, orderDto.Message);

            _db.Orders.Add(order);
            _db.SaveChanges();

            return order;
        }

        public List<Order> GetOrdersByUser(Guid userGuid)
        {
            var user = _userService.GetUser(userGuid);

            return _db.Orders.Where(x => x.BuyerId == user.Id)?.ToList();
        }

        public Order ModifyStatus(int id, OrderStatus status, Guid userGuid)
        {
            var user = _userService.GetUser(userGuid);

            /*if (user.Role != Role.Dispatcher)
            {
                throw new UnauthorizedAccessException("You can't modify this order's status!");
            }*/

            var order = _db.Orders.Find(id);

            if (order == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            order.Status = status;

            _db.SaveChanges();

            return order;
        }
    }
}