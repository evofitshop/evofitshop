﻿using Evoshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Evoshop.Services;
using Microsoft.Ajax.Utilities;
using System.ComponentModel.Composition;
using Evoshop.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    public class CartService : ApiController
    {
        [Import]
        private IUserContext db;
        public string SessionKey { get; set; }
        [Import]
        private ISessionService sessionService;
        public List<Product> Cart;

        public CartService(Guid guid)
        {
           // var id = sessionService.GetUser(guid).Id;
           // var _Cart = sessionService.GetUser(guid).Cart.ToList();
        }

        public void AddToCart(int product_id, string product_name, int product_quantity, int product_price)
        {
            SessionKey = GetCartId();
            //var cartItem = db.Products.Where(i => i.Id == product_id).FirstOrDefault();

            //if (Cart.Contains(cartItem))
            //{
            //    cartItem._quantity++;
            //}
            //else
            //{
            //    Cart.Add(cartItem);
            //}
        }

        public void RemoveFromCart(int product_id, string product_name, int product_quantity, int product_price)
        {
            SessionKey = GetCartId();

            //var cartItem = db.Products.Where(i => i.Id == product_id).FirstOrDefault();
            //if (Cart.Count == 0)
            //{
            //    Console.WriteLine("A kosár üres");
            //}
            //else
            //{
            //    if (Cart.Contains(cartItem))
            //    {
            //        cartItem._quantity = cartItem._quantity - 1;
            //    }
            //    else
            //    {
            //        Cart.Remove(cartItem);
            //    }
            //}
        }

        public void DeleteCart()
        {
            SessionKey = GetCartId();

            Cart.Clear();
        }

        /*public double CalculateTotal()
        {
            double total = 0;
                foreach (Product item in Cart)
                {
                    total += (item._quantity * item._price);
                }
            return Math.Round(total);
        }*/

        private string GetCartId()
        {
            if (System.Web.HttpContext.Current.Session[SessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace( System.Web.HttpContext.Current.User.Identity.Name ))
                {
                        System.Web.HttpContext.Current.Session[SessionKey] = System.Web.HttpContext.Current.User.Identity.Name;
                }
                else
                {     
                        Guid tempCartId = Guid.NewGuid();
                        System.Web.HttpContext.Current.Session[SessionKey] = tempCartId.ToString();
                }
            }
            return System.Web.HttpContext.Current.Session[SessionKey].ToString();
        }
    }
}