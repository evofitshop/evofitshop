﻿using Evoshop.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;

namespace Evoshop.Services
{
    [Export(typeof(ISessionService)), PartCreationPolicy(CreationPolicy.Shared)]
    public class SessionService : ISessionService
    {
        private Dictionary<Guid, int> _sessionRepository = new Dictionary<Guid, int>();

        public Guid Login(int userId)
        {
            var guid = Guid.NewGuid();

            _sessionRepository.Add(guid, userId);

            Debug.WriteLine($"Successful login for GUID {guid}, user ID {userId}.");

            return guid;
        }

        public bool Logout(Guid guid)
        {
            if (!_sessionRepository.Remove(guid))
            {
                Debug.WriteLine($"The user can not be logged out because the GUID does not exists in the session repository. GUID: {guid}");
                return false;
            }

            Debug.WriteLine($"Successful logout for GUID {guid}.");
            return true;
        }

        public bool IsLoggedIn(int userId)
        {
            return _sessionRepository.ContainsValue(userId);
        }

        public bool IsLoggedIn(Guid guid)
        {
            return _sessionRepository.ContainsKey(guid);
        }

        public Guid GetGuid(int userId)
        {
            if (_sessionRepository.ContainsValue(userId))
            {
                return _sessionRepository.First(p => p.Value == userId).Key;
            }
            else
            {
                throw new Exception("The user can not be found in the session repository.");
            }
        }

        public int GetUser(Guid guid)
        {
            if (_sessionRepository.ContainsKey(guid))
            {
                return _sessionRepository.First(p => p.Key == guid).Value;
            }
            else
            {
                throw new Exception("The GUID can not be found in the session repository.");
            }
        }
    }
}