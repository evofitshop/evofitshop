﻿using Evoshop.Interfaces;
using Evoshop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;

namespace Evoshop.Services
{
    [ExcludeFromCodeCoverage]
    public class RatingService
    {
        [Import]
        private IUserContext db;
        List<Rating> rating = new List<Rating>();

        public RatingService()
        {
            CompositionHelper.CompositionContainer.SatisfyImportsOnce(this);
        }

        public List<Rating> GetRating(int foodId)
        {
            //var ratings = db.Ratings.Where(rating => rating.FoodId == foodId);
            //rating.Add()
 
            //return ratings.First(rating => rating.FoodId == foodId).Value;
            rating = db.Ratings.Where(rating => rating.FoodId == foodId).ToList();
            return rating;

        }

        public int CreateRating(int value, int foodId, string comment)
        {
           
                if (1 <= value && value <= 5)
                {
                    var rating = new Rating(value, foodId, comment);

                    db.Ratings.Add(rating);
                    db.SaveChanges();

                    return rating.Id;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Out of range, value needs to be between 1 and 5");
                }
            
        }
        
        public int DeleteRatings(int id)
        {
            var ratings = db.Ratings.Where(rating => rating.Id == id);
            if (ratings.Any())
            {

                var rating = ratings.First();
                db.Ratings.Remove(rating);
                db.SaveChanges();

                return rating.Id;
            }
            else
            {
                throw new ArgumentException(nameof(id), "Error");
            }
        }
    
    }
}