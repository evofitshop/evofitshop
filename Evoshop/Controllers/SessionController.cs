﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Evoshop.Controllers
{
    public class SessionController : ApiControllerBase
    {
        [Import]
        private IUserService _userService;

        // Login
        [Route("api/Session/Login")]
        public HttpResponseMessage Post([FromBody]LoginUserDto dto)
        {
            if (dto == null ||
                string.IsNullOrWhiteSpace(dto.EmailAddress) ||
                string.IsNullOrWhiteSpace(dto.Password))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "The body DTO needs to be filled out.");
            }

            try
            {
                var user = _userService.Login(dto.EmailAddress.Trim(), dto.Password.Trim());
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        // Register
        [Route("api/Session/Register")]
        public HttpResponseMessage Post([FromBody]RegisterUserDto dto)
        {
            if (dto == null ||
                dto.Address == null ||
                string.IsNullOrWhiteSpace(dto.EmailAddress) ||
                string.IsNullOrWhiteSpace(dto.Password) ||
                string.IsNullOrWhiteSpace(dto.Name) ||
                string.IsNullOrWhiteSpace(dto.PhoneNumber) ||
                string.IsNullOrWhiteSpace(dto.Address.ZipCode) ||
                string.IsNullOrWhiteSpace(dto.Address.City) ||
                string.IsNullOrWhiteSpace(dto.Address.Street) ||
                string.IsNullOrWhiteSpace(dto.Address.HouseNumber))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "The body DTO needs to be filled out.");
            }

            try
            {
                var user = _userService.CreateUser(dto.EmailAddress.Trim(), dto.Password.Trim(), dto.Name.Trim(), dto.PhoneNumber.Trim(), dto.Address);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        // Logout
        [Route("api/Session/Logout")]
        public HttpResponseMessage Post()
        {
            if (Request.Headers.TryGetValues("guid", out var values) &&
                !string.IsNullOrWhiteSpace(values.First()) &&
                Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                _userService.Logout(parsedGuid);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }
        }
    }
}
