﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using Evoshop.Models;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Evoshop.Controllers
{
    [Route("api/Order")]
    public class OrderController : ApiControllerBase
    {
        [Import]
        private IOrderService _orderService;

        public HttpResponseMessage Post([FromBody] OrderDto dto)
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                var order = _orderService.PlaceOrder(dto, parsedGuid);

                return Request.CreateResponse(HttpStatusCode.OK, order);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponseMessage Get()
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                var orders = _orderService.GetOrdersByUser(parsedGuid);

                return Request.CreateResponse(HttpStatusCode.OK, orders);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponseMessage Patch(int id, OrderStatus status)
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                var order = _orderService.ModifyStatus(id, status, parsedGuid);

                return Request.CreateResponse(HttpStatusCode.OK, order);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}