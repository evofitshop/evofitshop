﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Evoshop.Controllers
{
    public class UserController : ApiControllerBase
    {
        [Import]
        private IUserService _userService;

        [Route("api/User")]
        public HttpResponseMessage Get()
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                var user = _userService.GetUser(parsedGuid);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // Edit User
        // api/User
        [Route("api/User")]
        public HttpResponseMessage Post([FromBody] RegisterUserDto dto)
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            if (dto == null ||
                dto.Address == null ||
                string.IsNullOrWhiteSpace(dto.EmailAddress) ||
                string.IsNullOrWhiteSpace(dto.Password) ||
                string.IsNullOrWhiteSpace(dto.Name) ||
                string.IsNullOrWhiteSpace(dto.PhoneNumber) ||
                string.IsNullOrWhiteSpace(dto.Address.ZipCode) ||
                string.IsNullOrWhiteSpace(dto.Address.City) ||
                string.IsNullOrWhiteSpace(dto.Address.Street) ||
                string.IsNullOrWhiteSpace(dto.Address.HouseNumber))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "The body DTO needs to be filled out correctly.");
            }

            try
            {
                var user = _userService.EditUser(parsedGuid, dto.EmailAddress.Trim(), dto.Password.Trim(), dto.Name.Trim(), dto.PhoneNumber.Trim(), dto.Address);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
