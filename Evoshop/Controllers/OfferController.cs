﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    public class OfferController : ApiController
    {

        private offerType _offertype;

        public void offerListing()
        {
            offerType _offertype = offerStatus();

            switch (_offertype)
            {
                case offerType.user:/*  lista a fiók legutóbb/leggyakrabban rendelt ételeiről */;
                    break;
                case offerType.guest:/* lista az egész oldalon leggyakrabban rendelt ételekről */;
                    break;
                default: /* lista az egész oldalon leggyakrabban rendelt ételekről */;
                    break;
            }
        }
        private offerType offerStatus()
        {
            offerType offer;
            bool isLoggedIn = (System.Web.HttpContext.Current.User != null) && (System.Web.HttpContext.Current.User.Identity.IsAuthenticated);

            if (isLoggedIn)
            {
                offer = offerType.user;
            } 
            else 
            {
                offer = offerType.guest;
            };
            return offer;
        }   
        public enum offerType
        {
            user,
            guest
        }
    }
}
