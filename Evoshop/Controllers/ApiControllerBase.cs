﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Evoshop.Controllers
{
    public class ApiControllerBase : ApiController
    {
        public ApiControllerBase()
        {
            CompositionHelper.CompositionContainer.SatisfyImportsOnce(this);
        }
    }
}