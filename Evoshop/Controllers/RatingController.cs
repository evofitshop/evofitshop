﻿using Evoshop.Interfaces;
using Evoshop.Models;
using Evoshop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RatingController : ApiController
    {
        private RatingService _ratingService;
        [Import]
        private IUserContext _db;
        

        public RatingController()
        {
         _ratingService = new RatingService();
         CompositionHelper.CompositionContainer.SatisfyImportsOnce(this);
        }

        [Route("api/Rating")]
        
        [HttpGet]
        public HttpResponseMessage ShowRating(int foodId)
        {
            var rating = _ratingService.GetRating(foodId);
            return Request.CreateResponse(HttpStatusCode.OK, rating);

        }

        [HttpPost]
        public HttpResponseMessage CreateRating([FromBody] Rating rating)
        {
            try
            {
                var result = _ratingService.CreateRating(rating.Value, rating.FoodId, rating.Comment);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (ArgumentOutOfRangeException e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch(ArgumentException e)
            { 
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/Rating/5
        public void Put()
        {
        }

        [HttpDelete]
        public HttpResponseMessage DeleteRating(int id)
        {
            try
            { 
                    var result = _ratingService.DeleteRatings(id);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (ArgumentOutOfRangeException e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch(ArgumentException e)
            { 
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
