﻿using Evoshop.Dtos;
using Evoshop.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Evoshop.Controllers
{
    public class CartController : ApiControllerBase
    {
        [Import]
        private ISessionService _sessionService;
        [Import]
        private IUserContext db;

        // GET: api/Cart
        public HttpResponseMessage Get()
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                var id = _sessionService.GetUser(parsedGuid);
                var user = db.Users.Where(u => u.Id == id).Include(u => u.Cart).Include(u => u.Cart.List).FirstOrDefault();

                return Request.CreateResponse(HttpStatusCode.OK, user.Cart);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        // POST: api/Cart
        public HttpResponseMessage Post([FromBody]CartDto dto)
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                 string.IsNullOrWhiteSpace(values.First()) ||
                    !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }
            try
            {
                if (dto == null || dto.List == null || dto.Id == null)
                {
                    throw new Exception("The DTO cant be empty");
                }

                var id = _sessionService.GetUser(parsedGuid);

                var user = db.Users.Where(u => u.Id == id).Include(u => u.Cart).Include(u => u.Cart.List).FirstOrDefault();

                user.Cart.List = dto.List;

                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, user.Cart);
                
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }

        }
    }
}
