﻿using Evoshop.Interfaces;
using Evoshop.Models;
using Evoshop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Evoshop.Controllers
{

    //[ExcludeFromCodeCoverage]
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CalorieController : ApiControllerBase
    {
        [Import]
        private IUserContext _db;
        [Import]
        private ICalorieService _calorieService;


        [Route("api/Calorie")]
        public HttpResponseMessage Post([FromBody] CalculateCalorie calorie)
        {
            var result = _calorieService.CreateCalorie(calorie);
            return Request.CreateResponse(HttpStatusCode.OK, result);

        }
        /*public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }*/

        [Route("api/Calorie")]
        public HttpResponseMessage Get()
        {
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                var user = _calorieService.GetUser(parsedGuid);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // POST: api/Calorie


        // PUT: api/Calorie/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Calorie/5
        public void Delete(int id)
        {
        }
    }
}
