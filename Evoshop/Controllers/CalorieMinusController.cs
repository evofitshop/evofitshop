﻿using Evoshop.Interfaces;
using Evoshop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CalorieMinusController : ApiController
    {
        [Import]
        private IUserContext _db;

        //copied
        [Import]
        private ISessionService sessionService;

        public int GetUser(Guid guid)
        {
            var userId = sessionService.GetUser(guid);
            return _db.Users.First(u => u.Id == userId).Id;
        }

        public CalorieMinusController()
        {
            CompositionHelper.CompositionContainer.SatisfyImportsOnce(this);
        }

        [Route("api/Minus")]

        [HttpGet]
        public HttpResponseMessage ShowCalorie(/*[FromBody]int id*/)
        {
            //copied
            if (!Request.Headers.TryGetValues("guid", out var values) ||
                string.IsNullOrWhiteSpace(values.First()) ||
                !Guid.TryParse(values.First().Trim(), out var parsedGuid))
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "The GUID is invalid.");
            }

            try
            {
                float result = 0;
                var userid = GetUser(parsedGuid);
                var username = _db.Users.First(user => user.Id == userid).Name;
                if(_db.Caloriedata.First(nm => nm.Name == username).CalculatedCalorie > 0)
                {
                    result = _db.Caloriedata.First(nm => nm.Name == username).CalculatedCalorie;
                }

                CalorieMinus Minus = new CalorieMinus();
                Minus.Name = username;
                if (result != null)
                {
                    Minus.calorie = result;
                }
                else
                {
                    Minus.calorie = 0;
                }
                //Minus.remainingcalorie = _db.Caloriedata.First(nm => nm.Name == username).RemainingCalorie;
                return Request.CreateResponse(HttpStatusCode.OK, Minus);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }

            /*var username = _db.Users.First(user => user.Id == id).Name;
            var result = _db.Caloriedata.First(nm => nm.Name == username).CalculatedCalorie;
            CalorieMinus Minus = new CalorieMinus();
            Minus.Name = username;
            Minus.calorie = result;
            //Minus.remainingcalorie = _db.Caloriedata.First(nm => nm.Name == username).RemainingCalorie;
            return Request.CreateResponse(HttpStatusCode.OK, Minus);*/
        }

        [HttpPost]
        public HttpResponseMessage CalorieMinus(int id, int foodId)
        {
            try
            {
                var username = _db.Users.First(user => user.Id == id).Name;
                var foodCalorie = _db.Foods.First(food => food.Id == foodId).calorie;
                var clr = _db.Caloriedata.First(nm => nm.Name == username).CalculatedCalorie;
                var result = clr - foodCalorie;
                _db.Caloriedata.First(nm => nm.Name == username).RemainingCalorie = (int)result;

                var datecheck = _db.Caloriedata.First(nm => nm.Name == username).Time;

                if (datecheck.AddDays(1) == DateTime.Today)
                {
                    _db.Caloriedata.First(nm => nm.Name == username).Time = DateTime.Today;
                    _db.Caloriedata.First(nm => nm.Name == username).RemainingCalorie = clr;
                }

                _db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK,result);
            }
            catch (ArgumentOutOfRangeException e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (ArgumentException e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}