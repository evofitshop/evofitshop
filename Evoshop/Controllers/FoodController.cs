﻿using Evoshop.Interfaces;
using Evoshop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Evoshop.Controllers
{
    public class FoodController : ApiControllerBase
    {
        List<Product> foods = new List<Product>();

        [Import]
        private IUserContext _db;

        [Route("api/Food/Get")]
        [HttpGet]
        public HttpResponseMessage getFoodData()
        {
            foods = _db.Products.ToList();
            List<Food> kajak = _db.Foods.ToList();
            DateTime today = DateTime.Today;
            DateTime date = kajak[33].addTime;
            List<Food> levesek = new List<Food>();
            List<Food> foetel = new List<Food>();
            List<Food> desszert = new List<Food>();
            Random rnd = new Random();
            
            for (int j = 33; j < 40; j++)
            {
                if (date.AddDays(7) <= today)
                {
                    for (int i = 0; i < kajak.Count; i++)
                    {
                        if ((int)kajak[i].type == 3 && kajak[i].Id != 34)
                        {
                            levesek.Add(kajak[i]);
                        }
                    }
                    Food item = levesek[rnd.Next(0, levesek.Count - 1)];

                    for (int i = 0; i < kajak.Count; i++)
                    {
                        if (((int)kajak[i].type == 1 || (int)kajak[i].type == 2 || (int)kajak[i].type == 8 || (int)kajak[i].type == 5 || (int)kajak[i].type == 4) && kajak[i].Id != 34)
                        {
                            foetel.Add(kajak[i]);
                        }
                    }
                    Food item2 = foetel[(rnd.Next(0, foetel.Count - 1))];

                    for (int i = 0; i < kajak.Count; i++)
                    {
                        if ((int)kajak[i].type == 0 && kajak[i].Id != 34)
                        {
                            desszert.Add(kajak[i]);
                        }
                    }
                    Food item3 = desszert[(rnd.Next(0, desszert.Count - 1))];

                    string tooltip = item.name + ",\n" + item2.name + ",\n" + item3.name;
                    float kaloria = item.calorie + item2.calorie + item3.calorie;

                    _db.Foods.First(food => food.Id == (j+1)).topping = tooltip;
                    _db.Foods.First(food => food.Id == (j+1)).calorie = kaloria;
                    _db.Foods.First(food => food.Id == (j+1)).addTime = kajak[j].addTime.AddDays(7);
                    _db.SaveChanges();
                }
            }
            
            return Request.CreateResponse(HttpStatusCode.OK, foods);


        }
        



        public FoodController() : base()
        {
            InitializeFoodTable();
        }

        [ExcludeFromCodeCoverage]
        private void InitializeFoodTable()
        {
            if (_db.Products.Count() == 0)
            {
                var foodList = new List<Product>
                {
                    new Food(Type.koret, "Sültkrumpli", "Sültkrupmli", 290, foodType.vega, DateTime.Parse("2020/05/18")),
                    new Pizza(890, 1650, Type.pizza, "Hawaii Pizza", "paradicsomos alap, sonka, sajt, ananász", 1190, foodType.husos, DateTime.Parse("2020/05/27")),
                    new Pizza(990, 1690, Type.pizza, "Magyaros Pizza", "paradicsomos alap, kolbász, sajt, paradicsom, sonka", 1290, foodType.husos, DateTime.Parse("2020/05/25")),
                    new Pizza(790, 1490, Type.pizza, "Vega Pizza", "paradicsomos alap, sajt, paradicsom, kukorica, uborka", 1190, foodType.vega, DateTime.Parse("2020/05/19")),
                    new Pizza(690, 1290, Type.pizza, "Margherita Pizza", "paradicsomos alap, paradicsom, oregano, bazsalikom, sajt", 990, foodType.vega, DateTime.Parse("2020/04/29")),
                    new Food(Type.desszert, "Csoki torta", "csoki bevonat, piskóta, csokis krém", 390, foodType.vega, DateTime.Parse("2020/05/18")),
                    new Food(Type.desszert, "Kókusz kocka", "Kókuszos krém, piskóta", 290, foodType.vega, DateTime.Parse("2020/05/30")),
                    new Food(Type.gyros, "Gyros tál", "Gyros hús, szósz, zöldségek, sültkrumpli", 1390, foodType.husos, DateTime.Parse("2020/05/17")),
                    new Food(Type.gyros, "Gyros pitában", "Pita, gyros hús, zöldségek, szósz", 690, foodType.husos, DateTime.Parse("2020/04/28")),
                    new Food(Type.gyros, "Vega gyros tál", "Zöldségek, sültkrumpli, szósz", 1190, foodType.vega, DateTime.Parse("2020/05/12")),
                    new Food(Type.gyros, "Vega gyros pitában", "Zöldségek, pita, szósz", 490, foodType.vega, DateTime.Parse("2020/05/14")),
                    new Food(Type.hamburger, "Hamburger", "Buci, húspogácsa, zöldségek, ketchup, mustár, majonéz", 590, foodType.husos, DateTime.Parse("2020/04/29")),
                    new Food(Type.hamburger, "Extra hamburger", "Buci, 2 db húspogácsa, sajt, zöldségek, ketchup, mustár, majonéz", 790, foodType.husos, DateTime.Parse("2020/05/03")),
                    new Food(Type.hamburger, "Hamburger menü", "Extra hamburger (buci, 2 db húspogácsa, sajt, zöldségek, ketchup, mustár, majonéz), sültkurmpli, cola", 1290, foodType.husos, DateTime.Parse("2020/05/19")),
                    new Food(Type.leves, "Húsleves", "Húsleves zöldségekkel, tészta", 790, foodType.husos, DateTime.Parse("2020/04/16")),
                    new Food(Type.leves, "Gyümölcsleves", "Gymülcsleves", 790, foodType.vega, DateTime.Parse("2020/05/19")),
                    new Food(Type.salata, "Cézár saláta", "Saláta, uborka, paradicsom, csirkemell, pirított kenyérkockák, öntet", 690, foodType.husos, DateTime.Parse("2020/06/01")),
                    new Food(Type.salata, "Saláta", "Saláta, öntet, pirított kenyérkockák", 390, foodType.vega, DateTime.Parse("2020/05/21")),
                    new Food(Type.sushi, "Sushi", "Rizs, lazac, uborka, paprika, avokádó", 990, foodType.husos, DateTime.Parse("2020/06/02")),
                    new Food(Type.foetel, "Rántott sajt", "Rántott sajt, sültkrumpli, tartármártás", 1190, foodType.vega, DateTime.Parse("2020/05/29")),
                    new Food(Type.foetel, "Rántott hús", "Rántott csirkemell, sültkrumpli", 1290, foodType.husos, DateTime.Parse("2020/05/30")),
                    new Food(Type.szendvics, "Sonkás szendvics", "Zsömle, sonka, vaj, sajt, saláta, paradicsom", 490, foodType.husos, DateTime.Parse("2020/04/01")),
                    new Food(Type.szendvics, "Vega szendvics", "Zsömle, sajt, paradicsom, saláta, vaj", 390, foodType.vega, DateTime.Parse("2020/05/12")),
                    new Food(Type.teszta, "Spagetti", "Tészta, darált marhahús, bolognai szósz, sajt", 1190, foodType.husos, DateTime.Parse("2020/03/29")),
                    new Food(Type.teszta, "Mákos tészta", "Tészta, mák, cukor", 790, foodType.vega, DateTime.Parse("2020/05/17")),
                    new Food(Type.eloetel, "Rántott gombafej tartármártással", "Gomba, tartármártás, sültkrumpli", 790, foodType.vega, DateTime.Parse("2020/05/24")),
                    new Food(Type.eloetel, "Rántott hagymakarika tartármártással", "Hagymakarika, tartármártás, sültkrumpli", 790, foodType.vega, DateTime.Parse("2020/05/19")),
                    new Food(Type.koret, "Rizs", "Rizs", 290, foodType.vega, DateTime.Parse("2020/05/04")),
                    new Food(Type.savanyusag, "Csalamádé", "Csalamádé", 290, foodType.vega, DateTime.Parse("2020/05/19")),
                    new Food(Type.savanyusag, "Uborka", "Uborka", 290, foodType.vega, DateTime.Parse("2020/05/18")),
                    new Food(Type.szosz, "Csípős", "Csípős szósz", 190, foodType.vega, DateTime.Parse("2020/05/16")),
                    new Food(Type.szosz, "Fokhagymás", "Fokhagymás szósz", 190, foodType.vega, DateTime.Parse("2020/05/25")),
                    new Food(Type.foetel, "Brassói aprópecsenye", "Sertés hús, szalonna, hagyma, paprika, fokhagyma, sültkrumpli", 1990, foodType.husos, DateTime.Parse("2020/05/16"))
                };

                _db.Products.AddRange(foodList);
                _db.SaveChanges();
            }
        }
    }
}
