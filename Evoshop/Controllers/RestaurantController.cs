﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using Evoshop.Controllers;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    class RestaurantController
    {
        private List<Restaurant> restaurantList = new List<Restaurant>();
        
        public RestaurantController(List<Restaurant> r)
        {
            this.restaurantList = r;
        }

        public void addRestaurant(Restaurant r)
        {
            bool isThere = false;
            for(int i = 0; i < restaurantList.Count; i++)
            {
                if(restaurantList[i] == r)
                {
                    isThere = true;
                }
            }
            if(!isThere)
            {
                restaurantList.Add(r);
            }
            else
            {
                Console.WriteLine("Az étterem már szerepel a listában!");
            }
        }

        public void removeRestaurant(Restaurant r)
        {
            if(restaurantList.Count == 0)
            {
                Console.WriteLine("Nincs még étterem a listában!");
            }
            else
            {
                bool finished = false;
                int i = 0;
                do
                {
                    if (restaurantList[i] == r)
                    {
                        restaurantList.Remove(r);
                        Console.WriteLine("A megadott étterem törölve lett a listából!");
                        finished = true;
                    }
                    else
                    {
                        i++;
                    }
                } while (!finished || i < restaurantList.Count);
                if(!finished)
                {
                    Console.WriteLine("A megadott étterem nem szerepelt a listában!");
                }
            }

        }
    }
}