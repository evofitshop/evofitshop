﻿using Evoshop.DataAccess;
using Evoshop.Services;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;

namespace Evoshop
{
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public static class CompositionHelper
    {
        public static CompositionContainer CompositionContainer;

        public static void InitializeMef()
        {
            var catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));

            CompositionContainer = new CompositionContainer(catalog, true);
        }
    }
}