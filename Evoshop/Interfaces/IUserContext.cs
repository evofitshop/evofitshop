﻿using Evoshop.Controllers;
using Evoshop.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Evoshop.Interfaces
{
    public interface IUserContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Address> Addresses { get; set; }
        DbSet<Rating> Ratings { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Food> Foods { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<Cart> Carts { get; set; }
        DbSet<Cart> GuestCarts { get; set; }
        DbSet<Pizza> Pizzas { get; set; }

        DbSet<CalculateCalorie> Caloriedata { get; set; }

        int SaveChanges();
        DatabaseFacade Database { get; }
    }
}