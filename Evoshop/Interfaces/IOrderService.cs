﻿using Evoshop.Dtos;
using Evoshop.Models;
using System;
using System.Collections.Generic;

namespace Evoshop.Interfaces
{
    interface IOrderService
    {
        Order PlaceOrder(OrderDto orderDto, Guid userGuid);

        List<Order> GetOrdersByUser(Guid userGuid);

        Order ModifyStatus(int id, OrderStatus status, Guid userGuid);
    }
}
