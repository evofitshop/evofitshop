﻿using Evoshop.Dtos;
using Evoshop.Models;
using System;

namespace Evoshop.Interfaces
{
    public interface IUserService
    {
        Guid CreateUser(string emailAddress, string password, string name, string phoneNumber, AddressDto addressDto);
        User GetUser(Guid guid);
        Guid Login(string emailAddress, string password);
        User EditUser(Guid userSession, string emailAddress, string password, string name, string phoneNumber, AddressDto addressDto);
        bool Logout(Guid guid);
    }
}