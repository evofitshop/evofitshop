﻿using System;

namespace Evoshop.Interfaces
{
    public interface ISessionService
    {
        Guid GetGuid(int userId);
        int GetUser(Guid guid);
        bool IsLoggedIn(Guid guid);
        bool IsLoggedIn(int userId);
        Guid Login(int userId);
        bool Logout(Guid guid);
    }
}