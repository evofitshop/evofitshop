﻿using Evoshop.Models;
using System;

namespace Evoshop.Interfaces
{
    public interface ICalorieService
    {
        int CreateCalorie(CalculateCalorie calorie);
        CalculateCalorie GetUser(Guid guid);
    }
}