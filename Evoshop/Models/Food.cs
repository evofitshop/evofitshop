﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    public class Food : Product
    {
        public Type type { get; set; }
        public string name { get; set; }
        public string topping { get; set; }
        public int price { get; set; }
        public foodType FoodType { get; set; }
        public DateTime addTime { get; set; }
        public string plusTopping { get; set; }
        public float calorie { get; set; }
        //Kivettem a private-okat, mert a FoodController-ben nem látta a változókat, date ideiglenesen kivéve, nem tudom hogy kell megani az értéket.

        public Food() : base () { }

        public Food(Type type, string name, string topping, int price, foodType FoodType, DateTime addTime) : base ()
        {
            this.type = type;
            this.name = name;
            this.topping = topping;
            this.price = price;
            this.FoodType = FoodType;
            this.addTime = addTime;
        }
        public Food(Type type, string name, string topping, int price, foodType FoodType, DateTime addTime, float calorie) : base()
        {
            this.type = type;
            this.name = name;
            this.topping = topping;
            this.price = price;
            this.FoodType = FoodType;
            this.addTime = addTime;
            this.calorie = calorie;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Food p = obj as Food;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Id == p.Id) && (type == p.type) && (name == p.name) && (topping == p.topping) && (price == p.price);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ type.GetHashCode() ^ name.GetHashCode() ^ topping.GetHashCode() ^ price.GetHashCode();
        }

    }
    
    public enum Type
    {
        desszert,
        gyros,
        hamburger,
        leves,
        pizza,
        salata,
        sushi,
        szendvics,
        teszta,
        foetel,
        eloetel,
        koret,
        szosz,
        savanyusag
    }
}
