﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    class Restaurant
    {
        private string name { get; set; }
        public List<Food> foodList = new List<Food>();
        private string phoneNumber { get; set; }
        private string email { get; set; }
        private string address { get; set; }
        private int deliveryFee { get; set; }

    public Restaurant(string name, List<Food> foods, string phoneNumber, string email, string address, int deliveryFee)
        {
            this.name = name;
            this.foodList = foods;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.address = address;
            this.deliveryFee = deliveryFee;
        }

        public void addFood(Food f)
        {
            bool alreadyThere = false;
            if(foodList.Count != 0) //Ha nincs egy étel sem a listában, akkor csak adja hozzá
            {
                //Ha van már étel a listában, akkor vizsgálja meg, hogy ez az étel szerepel-e már a listában

                for (int i = 0; i < foodList.Count; i++)
                {
                    if (foodList[i] == f)
                    {
                        alreadyThere = true;
                    }
                }
                //Ha nem szerepel, akkor adja hozzá
                if (!alreadyThere)
                {
                    foodList.Add(f);
                }
                //Ha szerepel, akkor jelezze, hogy már ott van
                else
                {
                    Console.WriteLine("Az etel mar szerepel a listan!");
                }
            }
            else
            {
                foodList.Add(f);
            } 
        }

        public void removeFood(Food f)
        {
            //Ha a lista üres, akkor ne kezdje el vizsgálni
            if (foodList.Count == 0)
            {
                Console.WriteLine("Nem lehet törölni az üres listából!");
            }
            //Ha nem üres, akkor vizsgálja meg, hogy az étel szerepel-e a listán, ha igen akkor törölje
            else
            {
                bool finished = false;
                int i = 0;
                do
                {
                    if (foodList[i] == f)
                    {
                        foodList.Remove(f);
                        Console.WriteLine("A megadott étel törölve lett a listából!");
                        finished = true;
                    }
                    else
                    {
                        i++;
                    }
                } while (!finished || i < foodList.Count);
                if(!finished)
                {
                    Console.WriteLine("A megadott étel nem szerepelt a listán!");
                }
            }
        }
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Restaurant p = obj as Restaurant;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (name == p.name) && (foodList == p.foodList) && (phoneNumber == p.phoneNumber) && (email == p.email) && (address == p.address) && (deliveryFee == p.deliveryFee);
        }

        public override int GetHashCode()
        {
            return name.GetHashCode() ^ foodList.GetHashCode() ^ phoneNumber.GetHashCode() ^ email.GetHashCode() ^ address.GetHashCode() ^ deliveryFee.GetHashCode();
        }
    }
}
