﻿using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Models
{
    [ExcludeFromCodeCoverage]
    public class Drink
    {
        public int Id { get; set; }
        public string DrinkName { get; set; }
        public int DrinkPrice { get; set; }
        public int DrinkQuantity { get; set; }

        public Drink(int id, string drinkname,int drinkprice, int drinkquantity)
        {
            this.Id = id;
            this.DrinkName = drinkname;
            this.DrinkPrice = drinkprice;
            this.DrinkQuantity = drinkquantity;
        }

        public Drink()
        {

        }

    }
}