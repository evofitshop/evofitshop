﻿using Evoshop.Dtos;
using Evoshop.Validators;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Models
{
    [ExcludeFromCodeCoverage]
    public class Address
    {
        public int Id { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string FloorAndDoor { get; set; }
        public string DoorBell { get; set; }

        public Address() { }

        public Address(string zipCode, string city, string street, string houseNumber, string floorAndDoor, string doorBell)
        {
            PostalCodeValidator.Validate(zipCode);

            ZipCode = zipCode;
            City = city;
            Street = street;
            HouseNumber = houseNumber;
            FloorAndDoor = floorAndDoor;
            DoorBell = doorBell;
        }

        public Address(AddressDto dto)
        {
            PostalCodeValidator.Validate(dto.ZipCode);

            ZipCode = dto.ZipCode.Trim();
            City = dto.City.Trim();
            Street = dto.Street.Trim();
            HouseNumber = dto.HouseNumber.Trim();

            FloorAndDoor = dto.FloorAndDoor?.Trim();
            DoorBell = dto.DoorBell?.Trim();
        }
    }
}