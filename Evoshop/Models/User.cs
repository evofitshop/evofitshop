﻿using Evoshop.Validators;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Models
{
    [ExcludeFromCodeCoverage]
    public class User
    {
        public int Id { get; private set; }
        public string EmailAddress { get; set; }
        // TODO: encrypt password
        public string Password { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public List<Address> Addresses { get; set; }
        public int Credits { get; set; }
        public List<Rating> Ratings { get; set; }
        public List<Order> Orders { get; set; }
        public Cart Cart { get; set; }
        //public Role Role { get; set; } = Role.Buyer;

        public User() { }

        public User(string emailAddress, string password, string name, string phoneNumber, Address address)
        {
            EmailValidator.Validate(emailAddress);
            PasswordValidator.Validate(password);
            PhoneNumberValidator.Validate(phoneNumber);

            EmailAddress = emailAddress;
            Password = password;
            Name = name;
            PhoneNumber = phoneNumber;
            Addresses = new List<Address>
            {
                address
            };
            Credits = 0;
            Ratings = new List<Rating>();
            Orders = new List<Order>();
            Cart = new Cart();
        }
    }

    public enum Role
    {
        Buyer,
        Dispatcher
    }
}