﻿using System.Collections.Generic;
using System.Linq;

namespace Evoshop.Models
{
    public class Order
    {
        public int Id { get; private set; }

        public List<OrderItem> Cart { get; private set; } = new List<OrderItem>();

        public int Price
        {
            get
            {
                return Cart.Sum(x => x.Price);
            }
        }

        public int BuyerId { get; private set; }

        public User Buyer { get; private set; }

        public OrderStatus Status { get; set; } = OrderStatus.Processing;

        public string Message { get; private set; }

        public int AddressId { get; private set; }

        public Address Address { get; private set; }

        public Order(List<OrderItem> cart, int buyerId, int addressId, string message = null)
        {
            Cart = cart;
            BuyerId = buyerId;
            Message = message;
            AddressId = addressId;
        }

        public Order()
        {
        }
    }

    public enum OrderStatus
    {
        Processing,
        InTransit,
        PickupAvailable,
        Delivered,
        Cancelled,
        Returned,
        Problem
    }
}