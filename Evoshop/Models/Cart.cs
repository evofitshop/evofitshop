﻿using Evoshop.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Models
{
    [ExcludeFromCodeCoverage]
    public class Cart
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public List<Product> List { get; set; }

        public Cart()
        {
            List = new List<Product>();
        }
    }
}