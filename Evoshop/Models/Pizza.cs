﻿using Evoshop.Controllers;
using System;
using System.Diagnostics.CodeAnalysis;
using Type = Evoshop.Controllers.Type;

namespace Evoshop.Models
{
    [ExcludeFromCodeCoverage]
    public class Pizza : Food
    {
        public int smallPrice { get; set; }
        public int largePrice { get; set; }

        public Pizza() : base() { }

        public Pizza(int smallPrice, int largePrice, Type type, string name, string topping, int mediumPrice, foodType FoodType, DateTime addTime) : base(type, name, topping, mediumPrice, FoodType, addTime)
        {
            this.smallPrice = smallPrice;
            this.largePrice = largePrice;
        }
    }
}