﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evoshop.Models
{
    public enum Size
    {
        Small = 0,
        Medium = 1,
        Large = 2
    }
}