﻿using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Models
{
    [ExcludeFromCodeCoverage]
    public class Rating
    {
        public int Id { get; set; } = 0;
        public int Value { get; private set; } = 0;
        public int FoodId { get; private set; } = 0;
        public string Comment { get; private set; } = "";

        public Rating(int value, int foodId, string comment)
        {
            Value = value;
            FoodId = foodId;
            Comment = comment;
        }
    }
}