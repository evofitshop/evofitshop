﻿using Evoshop.Controllers;

namespace Evoshop.Models
{
    public class OrderItem
    {
        public int Id { get; private set; }

        public Product Product { get; private set; }

        public Size Size { get; private set; }

        public int Quantity { get; private set; } = 1;

        public int Price
        {
            get
            {
                switch (Size)
                {
                    case Size.Small:
                        return ((Pizza)Product).smallPrice * Quantity;
                    case Size.Large:
                        return ((Pizza)Product).largePrice * Quantity;
                    default:
                        return ((Food)Product).price * Quantity;
                }
            }
        }

        public OrderItem(Product product, Size size = Size.Medium, int quantity = 1)
        {
            Product = product;
            Size = size;
            Quantity = quantity;
        }

        public OrderItem()
        {

        }
    }
}