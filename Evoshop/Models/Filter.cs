﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;

namespace Evoshop.Controllers
{
    [ExcludeFromCodeCoverage]
    public class Filter
    {
        //Egy adott feltételnek megfelelő ételeket kiszűri (pl. legyen rajta hagyma)
        public List<Food> filterFoodsByCriterion(string criterion, List<Food> foodList)
        {
            List<Food> filteredFoods = new List<Food>();
            for(int i = 0; i < foodList.Count; i++)
            {
                if(foodList[i].topping.Contains(criterion))
                {
                    filteredFoods.Add(foodList[i]);
                }
            }
            return filteredFoods;
        }


        //Kiszűri a vega vagy nem vega ételeket
        public List<Food> filterFoodsByType(foodType type, List<Food> foodList)
        {
            List<Food> filteredFoods = new List<Food>();
            for(int i = 0; i < foodList.Count; i++)
            {
                if(foodList[i].FoodType == type)
                {
                    filteredFoods.Add(foodList[i]);
                }
            }
            return filteredFoods;
        }


        //A megadott dátumnál később hozzáadott ételeket kiszűri
        public List<Food> newestFoods (List<Food> foodList)
        {
            Console.WriteLine("Add meg a dátumot, amelynél újabb ételeket akarod kilistázni! (NAP HÓNAP ÉV | A hónap neve angolul)");
            string dateString = Console.ReadLine();
            DateTime date = DateTime.Parse(dateString);
            List<Food> newestFoods = new List<Food>();
            if (foodList.Count == 0)
            {
                Console.WriteLine("Nincs étel a listában!");
            }
            else
            {
                for (int i = 0; i < foodList.Count; i++)
                {
                    if (DateTime.Compare(date, foodList[i].addTime) == 1)
                    {
                        newestFoods.Add(foodList[i]);
                    }
                }
            }
            return newestFoods;
        }
    }

    public enum foodType{
        husos,
        vega
    }
}