﻿using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Dtos
{
    [ExcludeFromCodeCoverage]
    public class AddressDto
    {
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string FloorAndDoor { get; set; }
        public string DoorBell { get; set; }
    }
}