﻿using Evoshop.Models;
using System.Collections.Generic;

namespace Evoshop.Dtos
{
    public class OrderDto
    {
        public List<OrderItemDto> Cart { get; set; }

        public string Message { get; set; }

        public int AddressId { get; set; }
    }

    public class OrderItemDto
    {
        public int ProductId { get; set; }

        public Size Size { get; set; }

        public int Quantity { get; set; }
    }
}