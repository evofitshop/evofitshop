﻿using Evoshop.Models;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Dtos
{
    [ExcludeFromCodeCoverage]
    public class RegisterUserDto
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public AddressDto Address { get; set; }
    }
}