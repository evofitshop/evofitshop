﻿using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Dtos
{
    [ExcludeFromCodeCoverage]
    public class LoginUserDto
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}