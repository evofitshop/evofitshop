﻿using Evoshop.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Evoshop.Dtos
{
    [ExcludeFromCodeCoverage]
    public class CartDto
    {
        public Guid Id { get; set; }
        public List<Product> List { get; set; }
    }
}