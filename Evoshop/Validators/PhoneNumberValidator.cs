﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Evoshop.Validators
{
    public class PhoneNumberValidator
    {
        private const string _pattern = @"^\+36(?:(?:(?:1|20|30|31|50|70)[1-9]\d{6})|[1-9]\d{7})$";

        public static void Validate(string value)
        {
            
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("The phone number can not be null or empty.");
            }

            value = value.Trim();

            if (!Regex.IsMatch(value, _pattern))
            {
                throw new ArgumentException("The phone number is not valid. The correct format is: +36301231234");
            }
        }
    }
}