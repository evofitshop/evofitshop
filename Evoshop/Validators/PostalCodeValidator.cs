﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evoshop.Validators
{
    public class PostalCodeValidator
    {
        private const int _minValue = 1000;
        private const int _maxValue = 9999;

        public static void Validate(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("The postal code can not be null or empty.");
            }

            value = value.Trim();

            if (value.Length != 4 || !int.TryParse(value, out int postalCode) || _minValue > postalCode || postalCode > _maxValue)
            {
                throw new ArgumentException("The postal code is not valid. The correct format is: between 1000 and 9999.");
            }
        }
    }
}