﻿using System;
using System.Text.RegularExpressions;

namespace Evoshop.Validators
{
    public class PasswordValidator
    {
        private const string _pattern = @"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}";

        public static void Validate(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("The password can not be null or empty.");
            }

            value = value.Trim();

            if (!Regex.IsMatch(value, _pattern))
            {
                throw new ArgumentException("The password is not valid. The correct format is: at least one number, one lowercase and one uppercase letter and at least six characters.");
            }
        }
    }
}