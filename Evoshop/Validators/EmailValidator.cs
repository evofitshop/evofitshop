﻿using System;
using System.Text.RegularExpressions;

namespace Evoshop.Validators
{
    public class EmailValidator
    {
        private const string _pattern = "^(([^<>()[\\]\\.,;:\\s@\"]+(\\.[^<>()[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

        public static void Validate(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("The email address can not be null or empty.");
            }

            value = value.Trim();

            if (!Regex.IsMatch(value, _pattern))
            {
                throw new ArgumentException("The email address is not valid. The correct format is: example@example.com.");
            }
        }
    }
}