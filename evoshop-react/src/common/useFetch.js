export const useFetch = {
    get,
    post,
    put,
    delete: _delete
};

const baseUrl = "https://localhost:44319/api/";

function get(url) {
    const requestOptions = {
        method: "GET",
        headers: {
            "guid": guid()
        },
    };
    return fetch(baseUrl + url, requestOptions).then(handleResponse);
}

function post(url, body) {
    const requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "guid": guid()
        },
        body: JSON.stringify(body)
    };
    return fetch(baseUrl + url, requestOptions).then(handleResponse);
}

function put(url, body) {
    const requestOptions = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            "guid": guid()
        },
        body: JSON.stringify(body)
    };
    return fetch(baseUrl + url, requestOptions).then(handleResponse);
}

function _delete(url) {
    const requestOptions = {
        method: "DELETE"
    };
    return fetch(baseUrl + url, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) {
            return Promise.reject(data);
        }

        return data;
    });
}

function guid() {
    return localStorage.getItem("guid") ?? undefined;
}