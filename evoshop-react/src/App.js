import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';
import FrontPage from './pages/FrontPage/FrontPage';
import SignInPage from './pages/SignInPage/SignInPage';
import LoginPage from './pages/LoginPage/LoginPage';
import PayPage from './pages/PayPage/PayPage';
import UserPage from './pages/UserPage/UserPage';
import EditCalorie from './pages/EditCaloriePage/EditCaloriePage';
import RatingPage from './pages/RatingPage/RatingPage';
import { useState, useEffect } from 'react';
import { useFetch } from './common/useFetch';

function App() {
  const [user, setUser] = useState(null);
  const [foods, setFoods] = useState(null);
  const [foodsError, setFoodsError] = useState(null);

  useEffect(() => {
    refreshUser();
  }, []);

  useEffect(() => {
    useFetch.get("Food/Get")
      .then((result) => {
        setFoods(result);
      })
      .catch((error) => {
        console.error("Food/Get", error);
        setFoodsError(error);
      });
  }, []);

  function onLogout() {
    setUser(null);
    localStorage.removeItem("guid", user);
  }

  function refreshUser() {
    const sessionId = localStorage.getItem("guid");
    if (sessionId) {
      useFetch.get("User")
        .then((user) => {
          setUser(user);
        })
        .catch((error) => {
          console.error(error);
          localStorage.removeItem("guid");
        });
    };
  }

  if (foodsError) {
    const message = new String(foodsError);
    return (
      <div className="App ErrorPage">
        <h1>Hoppá! Valami hiba történt. :(</h1>
        {message}
      </div>
    );
  } else if (!foods) {
    return (
      <div className="App LoadingScreen">
        <img src={"./img/loading.gif"} />
      </div>
    );
  } else {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route path="/login">
              <LoginPage refreshUser={refreshUser} />
            </Route>
            <Route path="/register">
              <SignInPage refreshUser={refreshUser} />
            </Route>
            <Route path="/pay">
              <PayPage />
            </Route>
            <Route path="/user">
              <UserPage />
            </Route>
            <Route path="/editCalorie">
              <EditCalorie />
            </Route>
            <Route path="/rating/:foodId">
              <RatingPage foods={foods} user={user} />
            </Route>
            <Route path="/">
              <FrontPage user={user} onLogout={onLogout} foods={foods} />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
