import React from 'react';
import {
    Link
} from "react-router-dom";
import { useState, useEffect } from 'react';
import { useFetch } from '../../common/useFetch';
import { useHistory } from "react-router-dom";


function InputContainer({ refreshUser }){
    const [error, setError] = useState(null);
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [emailError, setEmailError] = useState(null);
    const [pswError, setPswError] = useState(null);

    let history = useHistory();

    const submitButton = () =>{
        const data = 
            {
                EmailAddress: email,
                Password: password
            }
        checkFilledEmail();
        checkFilledPwd();
        useFetch.post("Session/Login",data) 
        .then((guid) =>{
        localStorage.setItem("guid",guid);
        refreshUser();

        //history.push("/");
        console.log(guid);
        })
        .catch((error) => {
            setError(error);  
        });
    
       
        console.log(data);
    }

    const [values, setValues] = useState({
        password: "",
        showPassword: false,
      });
      
      const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
      };
      
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };


    return (
        <form>
            <h1 class="title">Jelentkezz be</h1>
            <p id="backend-error"></p>
          
            <div class="login_input_container">
              <input class="o-input" type="text" name="emailAddress" id="email" onChange={e => setEmail(e.target.value)} value={email}  ></input>
              <label unselectable="on" onselectstart="return false;" for="email" class="input-label">E-mail cím</label>
              <p class="invalid-input" id="empty-email">{emailError}</p>
            </div>
          
            <div class="login_input_container">
              <input class="o-input o-input--password" type={values.showPassword ? "text" : "password"} name="password" id="password" onChange={e => setPassword(e.target.value)} value={password} ></input>
              <label unselectable="on" onselectstart="return false;" for="password" class="input-label">Jelszó</label>
              <div class="psw_eye" onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}><img src="/img/pswEye.png" ></img></div>
              <p class="invalid-input" id="empty-pwd">{pswError}</p>
            </div>
            <Link to="/" onClick={submitButton} class="logIn-button" id="login-button-send">Bejelentkezés</Link>
        </form>
    );

        
        
    function checkFilledEmail() {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

        if(email == null) {
            setEmailError("Az email mező kitöltése kötelező!");
        }
        else if (!pattern.test(email)) {
            setEmailError("Az email formátuma nem megfelelő!"); 
        }
        else {
            setEmailError(null);
        }
    }
        
    function checkFilledPwd() {
        if(password == null) {
           setPswError("A jelszó mező kitöltése kötelező!");
        } else {
            setPswError(null);
        }
    }
        
}

export default InputContainer;