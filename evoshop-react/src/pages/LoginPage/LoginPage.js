import React from 'react';
import LoginImage from './LoginImage';
import InputContainer from './InputContainer';
import './LoginPage.css';

function LoginPage({ refreshUser }) {
    return (
        <div className="Loginbody">
            <div className="login-container">
                <LoginImage />
                <InputContainer refreshUser={refreshUser} />
            </div>
        </div>
    );
}

export default LoginPage;
