import React, { pages } from 'react';
import {
    Link
} from "react-router-dom";
import RegisterImage from './SigninImage';
import InputContainer from './InputContainer';
import './SigninPage.css';
import { useState, useEffect } from 'react';
import { useFetch } from '../../common/useFetch';

function SignInPage({ refreshUser }) {

    const [user, setUser] = useState(null)

    useEffect(() => {
        const sessionId = localStorage.getItem("guid");
        console.log(sessionId);
        if (sessionId) {
            useFetch.get("User")
                .then((user) => {
                    setUser(user);
                })
                .catch((error) => {
                    console.error(error);
                    localStorage.removeItem("guid");
                });
        }
    }, []);

    return (
        <div className="Signinbody">
            <div className="signin-container">
                <RegisterImage />
                <InputContainer user={user} refreshUser={refreshUser} />
            </div>
        </div>
    );
}

export default SignInPage;
