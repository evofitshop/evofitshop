import React from 'react';
import {
    Link
} from "react-router-dom";
import EditData from './EditData';
import { useState, useEffect } from 'react';
import { useFetch } from '../../common/useFetch';
import { useHistory } from "react-router-dom"
import { render } from '@testing-library/react';

function InputContainer(props) {
    const [error, setError] = useState(null);
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [zip, setZip] = useState();
    const [city, setCity] = useState();
    const [street, setStreet] = useState();
    const [streetnumber, setStreetNumber] = useState();
    const [floor, setFloor] = useState();
    const [bell, setBell] = useState();

    const [usernameError, setUserNameError] = useState();
    const [passwordError, setPasswordError] = useState();
    const [emailError, setEmailError] = useState();
    const [phoneError, setPhoneError] = useState();
    const [zipError, setZipError] = useState();
    const [cityError, setCityError] = useState();
    const [streetError, setStreetError] = useState();
    const [streetnumberError, setStreetNumberError] = useState();


    let history = useHistory();

    const submitButton = () => {
        const data =
        {
            EmailAddress: email,
            Password: password,
            Name: username,
            PhoneNumber: phone,
            Address:
            {
                City: city,
                Street: street,
                HouseNumber: streetnumber,
                FloorAndDoor: floor,
                ZipCode: zip,
                DoorBell: bell
            }
        }
        mustFill();

        useFetch.post("Session/Register", data)
            .then((guid) => {
                localStorage.setItem("guid", guid);
                props.refreshUser();
                history.push("/");
                console.log(guid);
            })
            .catch((error) => {
                setError(error);
            })

        console.log(data);
    }

    const [values, setValues] = useState({
        password: "",
        showPassword: false,
    });

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };


    return (

        <form>
            {props.user ? (
                <div>
                    <EditData user={props} />
                </div>
            ) : (
                <div>
                    <h1 class="title">Regisztrálj</h1>
                    <p id="backend-error"></p>
                    <div class="signin-data-container">
                        <div class="signin-data-container1">
                            <form class="signin_input_container">
                                <input class="o-input" type="text" name="name" id="name" required="true" required onChange={e => setUserName(e.target.value)} value={username}></input>
                                <label for="name" class="input-label">Név *</label>
                                <p class="invalid-input" id="empty-name">{usernameError}</p>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input" type="tel" name="phone" id="phone" required onChange={e => setPhone(e.target.value)} value={phone} />
                                <label unselectable="on" onselectstart="return false;" for="phone" class="input-label">Telefonszám *<span
                                    class="c-input-with-label__additional-label l-throw-on-mobile"></span></label>
                                <p class="invalid-input" id="empty-phone">{phoneError}</p>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input" type="email" name="email" id="email" id="email" required onChange={e => setEmail(e.target.value)} value={email} />
                                <label unselectable="on" onselectstart="return false;" for="email" class="input-label">E-mail cím *</label>
                                <p class="invalid-input" id="empty-email">{emailError}</p>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input o-input--password" type={values.showPassword ? "text" : "password"} name="password" id="password" required onChange={e => setPassword(e.target.value)} value={password}></input>
                                <label unselectable="on" onselectstart="return false;" for="password" class="input-label">Jelszó *</label>
                                <div class="psw_eye" onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}><img src="/img/pswEye.png" ></img></div>
                                <p class="invalid-input" id="empty-pwd">{passwordError}</p>
                            </form>
                        </div>
                        <div class="signin-data-container2">
                            <form class="signin_input_container">
                                <input class="o-input" name="zip" id="zip" value="" required onChange={e => setZip(e.target.value)} value={zip}></input>
                                <label unselectable="on" onselectstart="return false;" for="zip" class="input-label">Ir.szám *</label>
                                <p class="invalid-input" id="empty-zip">{zipError}</p>
                            </form>
                            <form class="signin_input_container c-registration__city-select">
                                <input class="o-input citySelector ui-autocomplete-input" type="text" name="city" id="city" autocomplete="off" required onChange={e => setCity(e.target.value)} value={city}></input>
                                <label unselectable="on" onselectstart="return false;" for="city" class="input-label">Település *<span
                                    class="c-input-with-label__additional-label l-throw-on-mobile"></span></label>
                                <p class="invalid-input" id="empty-city">{cityError}</p>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input" type="text" name="street" id="street"
                                    value="" data-source="/front/address/street-list" autocomplete="off" required onChange={e => setStreet(e.target.value)} value={street}></input>
                                <label unselectable="on" onselectstart="return false;" for="street" class="input-label">Utca *<span
                                    class="c-input-with-label__additional-label l-throw-on-mobile"> (Ide csak utcanevetírj!)</span></label>
                                <p class="invalid-input" id="empty-street">{streetError}</p>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input" type="text" name="streetnumber" id="streetNumber" required onChange={e => setStreetNumber(e.target.value)} value={streetnumber}></input>
                                <label unselectable="on" onselectstart="return false;" for="streetNumber" class="input-label">Házszám *</label>
                                <p class="invalid-input" id="empty-streetNumber">{streetnumberError}</p>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input" type="text" name="floor" id="FloorAndDoor" onChange={e => setFloor(e.target.value)} value={floor}></input>
                                <label unselectable="on" onselectstart="return false;" for="floor" class="input-label">Emelet/ajtó</label>
                            </form>
                            <form class="signin_input_container">
                                <input class="o-input" type="text" name="bell" id="bell" onChange={e => setBell(e.target.value)} value={bell}></input>
                                <label unselectable="on" onselectstart="return false;" for="bell" class="input-label">Kapucsengő</label>
                            </form>
                        </div>
                    </div>
                    <Link onClick={submitButton} class="signIn-button" id="signIn-button-send" type="submit">Regisztráció</Link>
                </div>
            )
            };
        </form>
    );


    function mustFill() {
        if (checkFilledN() & checkFilledP() & checkFilledE() & checkFilledPwd() & checkFilledZ() & checkFilledC() & checkFilledS() & checkFilledSN()) {
            return true;
        }
        else {
            return false;
        }
    }

    function checkFilledN() {
        if (username == null) {
            setUserNameError("A név mező kitöltése kötelező!");
            return false;
        }
        else {
            setUserNameError(null);
            return true;
        }
    }

    function checkFilledP() {
        var re = /^\+36(?:(?:(?:1|20|30|31|50|70)[1-9]\d{6})|[1-9]\d{7})$/;
        if (phone == null) {
            setPhoneError("A telefonszám mező kitöltése kötelező!");
            return false;
        }
        else if (!re.test(phone)) {
            setPhoneError("Hibás telefonszám! Próbáld meg ilyen formában megadni: +36203176914");
            return false;
        }
        else {
            setPhoneError(null);
            return true;
        }
    }

    function checkFilledE() {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (email == null) {
            setEmailError("Az e-mail mező kitöltése kötelező!");
            return false;
        }
        else if (!re.test(email)) {
            setEmailError("Hibás e-mail cím! Próbáld meg ilyen formában megadni: valaki@evofittshop.hu");
            return false;
        }
        else {
            setEmailError(null);
            return true;
        }
    }

    function checkFilledPwd() {
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
        if (password == null) {
            setPasswordError("A jelszó mező kitöltése kötelező!");
            return false;
        }
        else if (!re.test(password)) {
            setPasswordError("A jelszónak 6 karakterből kell állnia, kell bele szám és kis és nagybetű!");
            return false;
        }
        else {
            setPasswordError(null);
            return true;
        }
    }

    function checkFilledZ() {


        if (zip == null) {
            setZipError("Az irányítószám mező kitöltése kötelező!");
            return false;
        }
        else {
            const digits_only = string => [...string].every(c => '0123456789'.includes(c));

            console.log(digits_only(zip));
            if (digits_only(zip) == false || zip <= 1000 || zip >= 9999) {
                setZipError("Az irányító számnak 4 számból kell állnia!");
                return false;
            }
            else {
                setZipError(null);
                return true;
            }
        }
    }

    function checkFilledC() {
        if (city == null) {
            setCityError("A település mező kitöltése kötelező!");
            return false;
        }
        else {
            setCityError(null);
            return true;
        }
    }

    function checkFilledS() {
        if (street == null) {
            setStreetError("Az utca mező kitöltése kötelező!");
            return false;
        }
        else {
            setStreetError(null);
            return true;
        }
    }

    function checkFilledSN() {
        if (streetnumber == null) {
            setStreetNumberError("A házszám mező kitöltése kötelező!");
            return false;
        }
        else {
            setStreetNumberError(null);
            return true;
        }
    }

}



export default InputContainer;