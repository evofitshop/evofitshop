import React, { useEffect } from 'react';
import { useState } from 'react';
import './StarRating.css';

function StarRating({ value, readonly, callback }) {
    const [rating, setRating] = useState(value);
    const [selection, setSelection] = useState(0);

    useEffect(() => {
        setRating(value);
    }, [value]);

    const hoverOver = event => {
        if (readonly) {
            return;
        }

        let val = 0;
        if (event &&
            event.target &&
            event.target.getAttribute('data-star-id')) {
            val = event.target.getAttribute('data-star-id');
        }
        setSelection(val);
    };

    const onClick = event => {
        if (readonly) {
            return;
        }
        const newRating = event.target.getAttribute('data-star-id') || rating;
        setRating(newRating);
        callback(newRating);
    };

    return (
        <div
            className="StarRating"
            onMouseOut={() => hoverOver(null)}
            onClick={onClick}
            onMouseOver={hoverOver}
        >
            {Array.from({ length: 5 }, (v, i) => (
                <Star
                    starId={i + 1}
                    key={`star_${i + 1}`}
                    marked={selection ? selection >= i + 1 : rating >= i + 1}
                />
            ))}
        </div>
    );
}

const Star = ({ marked, starId }) => {
    return (
        <span data-star-id={starId} className={"star" + (marked ? " marked" : "")} role="button">
            {'\u2605'}
        </span>
    );
};

export default StarRating;
