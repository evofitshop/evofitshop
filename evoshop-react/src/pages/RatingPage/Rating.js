import React from 'react';
import './Rating.css';
import StarRating from './StarRating';

function Rating(props) {
    return (
        <div className="Rating">
            <div>{props.rating.Comment}</div>
            <StarRating value={props.rating.Value} readonly={true}></StarRating>
        </div>
    );
}

export default Rating;
