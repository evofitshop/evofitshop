import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useParams } from 'react-router';
import { useFetch } from '../../common/useFetch';
import CreateRating from './CreateRating';
import Rating from './Rating';
import './RatingPage.css';

function RatingPage(props) {
    const { foodId } = useParams();
    const [food, setFood] = useState({});
    const [ratings, setRatings] = useState([]);

    useEffect(() => {
        const found = props.foods.find(food => food.Id == foodId);
        if (found && found != undefined) {
            setFood(found);
            useFetch.get("Rating?FoodId=" + foodId)
                .then(ratings => {
                    setRatings(ratings);
                });
        }
    }, [props.foods]);

    function onSubmit() {
        useFetch.get("Rating?FoodId=" + foodId)
            .then(ratings => {
                setRatings(ratings);
            });
    }

    return (
        <div className="RatingPage">
            <div className="RatingComponent">
                <div className = "RatingTitleComponent">
                <h1>Értékelés</h1>
                <h2>{food.name}</h2>
                </div>
                <div className="RatingsDataComponent">
                {
                    ratings.length
                    ? ratings.map((rating, i) => <Rating rating={rating} key={i}></Rating>)
                    : <div className="not-rated">Ezt a terméket még nem értékelték. :(</div>
                }
                </div>
                <div className = "CreateRatingComponent">
                {
                    props.user
                    ? <CreateRating food={food} onSubmitCallback={onSubmit}></CreateRating>
                    : <div className="not-rated">Az értékeléshez kérlek regisztrálj vagy jelentkezz be!</div>
                }
                </div>
            </div>
        </div>
    );
}

export default RatingPage;
