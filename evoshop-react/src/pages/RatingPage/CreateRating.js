import React from 'react';
import { useState } from 'react';
import { useFetch } from '../../common/useFetch';
import './CreateRating.css';
import StarRating from './StarRating';

function CreateRating({ food, onSubmitCallback }) {
    const [star, setStar] = useState(0);
    const [starError, setStarError] = useState();

    const [comment, setComment] = useState("");
    const [commentError, setCommentError] = useState();

    function onStarChange(value) {
        setStar(value);
    }

    function onCommentChange(event) {
        setComment(event.target.value);
    }

    function onSubmit(event) {
        event.preventDefault();
        
        if (!isFormValid()) {
            return;
        }

        useFetch.post("Rating/CreateRating", {
            Id: 0,
            Value: star,
            FoodId: food.Id,
            Comment: comment
        }).then(() => {
            onSubmitCallback();
            setStar(0);
            setComment("");
        }).catch(error => {
            console.error(error);
        });
    }

    function isFormValid() {
        setStarError();
        setCommentError();

        let valid = true;
        
        if (!star) {
            setStarError("Nem értékelheted 0 csillagra a terméket!");
            valid = false;
        }
        if (!comment) {
            setCommentError("Nem lehet üres a hozzászólás!");
            valid = false;
        }

        return valid;
    }

    return (
        <div className="CreateRating">
            <h3>Értékelés hozzáadása</h3>
            <form onSubmit={onSubmit}>
                <StarRating value={star} readonly={false} callback={onStarChange}></StarRating>
                <span className="rating-error">{starError}</span>
                <textarea maxLength="250" rows="3" value={comment} onChange={onCommentChange}></textarea>
                <span className="rating-error">{commentError}</span>
                <button id="rating-submit" type="submit">Hozzáadás</button>
            </form>
        </div>
    );
}

export default CreateRating;
