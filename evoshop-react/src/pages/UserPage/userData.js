import React from 'react';
import { useState, useEffect } from 'react';
import { useFetch } from '../../common/useFetch';
import userData from '../UserPage/userData';
import './user.css';
import {
    Link
} from "react-router-dom";

function UserData(){
    
    const [error, setError] = useState(null);

                const [username, setUserName] = useState();
                const [email, setEmail] = useState();
                const [phone, setPhone] = useState();
                const [zip, setZip] = useState();
                const [city, setCity] = useState();
                const [street, setStreet] = useState();
                const [streetnumber, setStreetNumber] = useState();
                const [floor, setFloor] = useState();
                const [bell, setBell] = useState();
                const [calorie, setCalorie] = useState(0);

     useEffect(() => {
        useFetch.get("Minus")
            .then((result) => {
                console.log(result);
                setCalorie(result.calorie);
            })
            .catch((error) => {
                setError(error);
            });
     }, []);


    useEffect(() => {
        useFetch.get("User")
            .then((result) => {
                console.log(result);
                setUserName(result.Name);
                setPhone(result.PhoneNumber);
                setEmail(result.EmailAddress);
                setZip(result.Addresses[0].ZipCode);
                setCity(result.Addresses[0].City);
                setStreet(result.Addresses[0].Street);
                setStreetNumber(result.Addresses[0].HouseNumber);
                setFloor(result.Addresses[0].FloorAndDoor);
                setBell(result.Addresses[0].DoorBell);
            })
            .catch((error) => {
                setError(error);
            });
        }, []);
        
                

              return(
                  <div>
                        <div class="user-data-container">
                                <div class="user-data-container1">
                                    <p class="name">Név: {username}</p>
                                    <p class="phone">Telefonszám: {phone}</p>
                                    <p class="email">E-mail: {email}</p>
                                    <p class="calorie">Napi kalóriamennyiség: {calorie} kcal  </p>
                                    {/*<p class="password">Jelszó: </p>*/}
                                   
                                </div>
                                <div class="user-data-container2">
                                    <p class="zip">Irányítószám: {zip}</p>
                                    <p class="city">Település: {city}</p>
                                    <p class="street">Utca: {street}</p>
                                    <p class="streetnumber">Házszám: {streetnumber}</p>
                                    <p class="FloorAndDoor">Emelet/ajtó: {floor}</p>
                                    <p class="bell">Kapucsengő: {bell}</p>
                                </div>
                        </div>   
                        <div>
                        <Link to="/editCalorie">
                                <a class="caloriedata-button">Kalória-adatok <br /> szerkesztése</a>                     
                        </Link>
                        <Link to="/register">
                                <a class="editdata-button">Személyes adatok <br /> szerkesztése</a>                     
                        </Link>
                        </div>
                    </div>      
              );
}

export default UserData;