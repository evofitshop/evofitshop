import React, {pages} from 'react';
import {
    Link
} from "react-router-dom";
import BackButton from './backButton';
import LoginUserImage from './loginUserImage';
import UserData from './userData';
import './user.css';
import { useState, useEffect } from 'react';
import { useFetch } from '../../common/useFetch';

function UserPage(){

    return(
    <div className="Userbody">
              <div class="user-container">
                    <BackButton/>
                    <LoginUserImage/>
                    <h1 class="title">Felhasználói adatok</h1>
                    <UserData/>    
                    {/*<Link  to="/register" class="signIn-button" id="signIn-button-send" type="submit">Szerkesztés</Link> */}
              </div> 
    </div>
    );
}

export default UserPage;