import React from 'react';

function LoginUserImage(){
        return (
              <div class="image-wrap">
              <img id="loginUser" src="/img/loginUser.png" class="user-image" alt=""></img>
          </div>
        );
}

export default LoginUserImage;