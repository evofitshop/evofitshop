import React from 'react';
import {
      Link
  } from "react-router-dom";

function backButtonImage(){
        return (
             <Link to="/">
                   <img src="/img/back.png" class="back" id="nav-back" alt=""></img>                     
             </Link> 
        );
}

export default backButtonImage;