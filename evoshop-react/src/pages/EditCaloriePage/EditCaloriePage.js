import React, {pages} from 'react';
import {
    Link
} from "react-router-dom";
import EvoshopImage from './evoshopImage';
import CalorieData from './calorieData';
import './editCalorie.css';
import { useState, useEffect } from 'react';
import { useFetch } from '../../common/useFetch';

function EditCaloriePage(){

    return(
    <div className="editCaloriebody">
              <div class="editCalorie-container">
                    <EvoshopImage/>
                    <h1 class="title">Kalória-adatok:</h1>
                    <CalorieData/>    
              </div> 
    </div>
    );
}

export default EditCaloriePage;