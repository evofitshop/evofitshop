import React from "react";
import { useState, useEffect } from "react";
import { useFetch } from "../../common/useFetch";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";

function CalorieData() {
  const [error, setError] = useState(null);
  const [name, setName] = useState();
  const [age, setAge] = useState();
  const [gender, setGender] = useState();
  const [height, setHeight] = useState();
  const [weight, setWeight] = useState();
  const [dreamWeight, setDreamWeight] = useState();
  const [activity, setActivity] = useState();
  const [goal, setGoal] = useState();

  const [errorAge, setErrorAge] = useState();
  const [errorHeight, setErrorHeight] = useState();
  const [errorWeight, setErrorWeight] = useState();
  const [errorDreamWeight, setErrorDreamWeight] = useState();

  useEffect(() => {
    useFetch.get("User")
        .then((result) => {
            console.log(result);
            setName(result.Name);
        })
        .catch((error) => {
            setError(error);
        });
    }, []);

  useEffect(() => {
    useFetch
      .get("Calorie")
      .then((result) => {
        if(result != null){
        console.log(result);
        //setName(result.Name);
        setGender(result.Gender);
        setAge(result.Age);
        setHeight(result.Height);
        setWeight(result.Weight);
        setDreamWeight(result.DreamWeight);
        setActivity(result.Activity);
        setGoal(result.Goal);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  let history = useHistory();

  const submitButton = () => {
    const data = {
      Name: name,
      Gender: gender,
      Age: age,
      Height: height,
      Weight: weight,
      DreamWeight: dreamWeight,
      Activity: activity,
      Goal: goal,
    };
    if (checkAll()) {
      useFetch
        .post("Calorie", data)
        .then((guid) => {
          history.push("./user");
          console.log(guid);
        })
        .catch((error) => {
          setError(error);
        });
    }
  };

  return (
    <div className="personal-data-container">
      <div className="personal-data-container1">
        <form className="input_container">
          <input
            className="o-input"
            type="text"
            required
            onChange={(e) => setAge(e.target.value)}
            value={age}
          ></input>
          <label className="input-label">Kor</label>
          <p className="invalid-input">{errorAge}</p>
        </form>
        <select
          className="gender-input"
          onChange={(e) => setGender(e.target.value)}
          value={gender}
        >
          <option defaultValue>Nem</option>
          <option value="0.9">Nő</option>
          <option value="1.3">Férfi</option>
        </select>

        <form className="input_container">
          <input
            className="o-input"
            type="height"
            required
            onChange={(e) => setHeight(e.target.value)}
            value={height}
          ></input>
          <label unselectable="on" className="input-label">
            Magasság
          </label>
          <p className="invalid-input">{errorHeight}</p>
        </form>
        <form className="input_container">
          <input
            className="o-input"
            type="weight"
            required
            onChange={(e) => setWeight(e.target.value)}
            value={weight}
          ></input>
          <label unselectable="on" className="input-label">
            Tömeg
          </label>
          <p className="invalid-input">{errorWeight}</p>
        </form>
      </div>

      <div className="personal-data-container2">
        <form className="input_container">
          <input
            className="o-input"
            type="weight"
            required
            onChange={(e) => setDreamWeight(e.target.value)}
            value={dreamWeight}
          ></input>
          <label unselectable="on" className="input-label">
            Álomtömeg
          </label>
          <p className="invalid-input">{errorDreamWeight}</p>
        </form>
        <div>
          <select
            className="gender-input"
            onChange={(e) => setActivity(e.target.value)}
            value={activity}
          >
            <option defaultValue>Aktivitás</option>
            <option value="1.2">
              Ha szinte csak ülsz egész nap, néha edzel
            </option>
            <option value="1.3">
              Ha szinte csak ülsz de picit mozogsz is, néha edzel
            </option>
            <option value="1.4">
              Ha sokat állsz és néha sétálsz, rendszeresen edzel
            </option>
            <option value="1.5">
              A sokat sétálsz a nap folyamán, rendszersen edzel
            </option>
            <option value="1.6">
              Ha kemény fizikai munkát végzel, rendszeresen edzel
            </option>
          </select>
        </div>
        <div>
          <select
            className="gender-input"
            onChange={(e) => setGoal(e.target.value)}
            value={goal}
          >
            <option defaultValue>Cél</option>
            <option value="-500">Zsírégetés</option>
            <option value="0">Egyik sem</option>
            <option value="500">Tömegnövelés</option>
          </select>
        </div>
      </div>
      <Link>
        <button onClick={submitButton} className="editCalorie-button">
          Adatok megadása
        </button>
      </Link>
    </div>
  );

  function checkAll() {
    if (checkAge() & checkHeight() & checkWeight() & checkDreamWeight()) {
      return true;
    } else {
      return false;
    }
  }

  function checkAge() {
    if (isNaN(age)) {
      setErrorAge("Nem megfelelő formátum!");
      return false;
    } else {
      setErrorAge(null);
      return true;
    }
  }

  function checkHeight() {
    if (isNaN(height)) {
      setErrorHeight("Nem megfelelő formátum!");
      return false;
    } else {
      setErrorHeight(null);
      return true;
    }
  }
  function checkWeight() {
    if (isNaN(weight)) {
      setErrorWeight("Nem megfelelő formátum!");
      return false;
    } else {
      setErrorWeight(null);
      return true;
    }
  }
  function checkDreamWeight() {
    if (isNaN(dreamWeight)) {
      setErrorDreamWeight("Nem megfelelő formátum!");
      return false;
    } else {
      setErrorDreamWeight(null);
      return true;
    }
  }
}
export default CalorieData;