import React from 'react';
import FoodCard from './FoodCard';

import './FoodList.css';

function FoodList({ foods }) {
    return (
        <div className="FoodList">
            {foods.map(food => (
                <FoodCard key={food.Id} food={food}/>
            ))}
        </div>
    );
}

export default FoodList;
