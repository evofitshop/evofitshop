import React, { useEffect, useState } from 'react';

function PlusFood({ callback }) {
    const categories = [
        { id: 6, name: "Sushi" },
        { id: 7, name: "Szendvics" },
        { id: 9, name: "Főétel" },
        { id: 10, name: "Előétel" },
        { id: 11, name: "Köret" },
        { id: 12, name: "Szósz" },
        { id: 13, name: "Savanyúság" },
    ];

    const [checkedState, setCheckedState] = useState(
        new Array(14).fill(false)
    );

    function onChange(position) {
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );

        setCheckedState(updatedCheckedState);

        callback(updatedCheckedState);
    }

    return (
        <div>
            <h2 className="food-category-title">Plusz étel:</h2>
            <div className="filter-box">
                {categories.map((category, i) => (
                    <div className="c-filter-box__checkbox" key={`checkbox-plus-food-${category.id}`}>
                        <input
                            className="PlusFoodCheckbox" type="checkbox" name="checkbox-plus-food"
                            id={`checkbox-plus-food-${category.id}`}
                            value={category.id}
                            checked={checkedState[category.id]}
                            onChange={() => onChange(category.id)}
                        ></input>
                        <label
                            className="PlusFoodLabel"
                            htmlFor={`checkbox-plus-food-${category.id}`}
                        >{category.name}</label>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default PlusFood;
