import React from "react";
import { Link } from "react-router-dom";
import { useFetch } from "../../common/useFetch";

function Navbar(props) {
    const logout = () => {
        useFetch.post("Session/Logout")
            .finally(() => {
                props.onLogout();
            });
    };

    return (
        <header className="header">
            <img src="img/logo.png" className="menu-logo" alt=""></img>
            {props.user ? (
                <div className="logged-in">
                    <p className="welcome-user">Üdvözöllek <b>{props.user.Name}</b>!</p>
                    <a>
                    <Link to="/user">
                        <img
                            id="loggedIn"
                            src="/img/logged-in.png"
                            className="logged-in__img"
                        ></img>
                    </Link>
                    </a>
                    <img
                        src="/img/sign_out.png"
                        className="logout__img"
                        alt=""
                        id="user-profile-logout"
                        onClick={logout}
                    ></img>
                    {/*<Link to="/calorie" className="login-button">
                        Kalória
                    </Link>*/}
                </div>
            ) : (
                <>
                    <Link to="/login" className="login-button">
                        Bejelentkezés
          </Link>
                    <Link to="/register" className="signin-button">
                        Regisztráció
          </Link>
                </>
            )}

            <div className="cart-wrap" id="cart-wrap">
                <div className="cart-img" href="#">
                    <img
                        src="/img/shopping-cart.svg"
                        className="cart-buttom"
                        id="cart-img"
                        alt=""
                    ></img>
                </div>
                <p id="shopping-cart-counter">0</p>
                <div className="cart-items-wrap" id="cart-items">
                    <div className="wrapper">
                        <div className="divider-cart-start div-transparent"></div>
                    </div>
                    <div id="cart-empty-item"></div>
                    <div className="cart-items-wrap__arrow"></div>
                    <div className="cart-pay-info">
                        <p>Fizetendő:</p>
                        <p id="cart-pay-amount">0</p>
                    </div>
                    <Link to="/pay" className="pay-button">
                        Fizetés
          </Link>
                </div>
            </div>
        </header>
    );
}

export default Navbar;
