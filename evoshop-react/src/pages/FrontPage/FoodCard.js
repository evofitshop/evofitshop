import React from 'react';
import { Link } from "react-router-dom";

import './FoodCard.css';

const categories = [
    { id: 0, name: "desszert", icon: "pie" },
    { id: 1, name: "gyros", icon: "burrito" },
    { id: 2, name: "hamburger", icon: "hamburger" },
    { id: 3, name: "leves", icon: "soup" },
    { id: 4, name: "pizza", icon: "pizza" },
    { id: 5, name: "salata", icon: "salad" },
    { id: 6, name: "sushi", icon: "fish" },
    { id: 7, name: "szendvics", icon: "sandwich" },
    { id: 8, name: "teszta", icon: "wheat" },
    { id: 9, name: "foetel", icon: "turkey" },
    { id: 10, name: "eloetel", icon: "bacon" },
    { id: 11, name: "koret", icon: "french-fries" },
    { id: 12, name: "szosz", icon: "pepper-hot" },
    { id: 13, name: "savanyusag", icon: "seedling" }
];

function FoodCard({ food }) {
    const icon = food.Id >= 34 && food.Id <= 40 ? "burger-soda" : categories[food.type].icon;
    const isPizza = food.type === 4;
    const isMenu = food.type === 14;
    var today = new Date();
    var dateTime = food.addTime.substring(0, 10);
    var parts = dateTime.split('-');
    var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
    var day = Math.floor((today - mydate) / (24 * 60 * 60 * 1000))
    var dailyId = 34 + day;
    const isToday = food.Id === dailyId;

    return (
        <div className="FoodCard">
            <i class="FoodCardInfo fa fa-info-circle">
                <span class="FoodCardInfoText">
                    <b>Összetevők: </b>{food.topping}
                    <br /><br />
                    <b>Kalória: </b>{food.calorie} kcal</span>
            </i>
            <>
                <i className={"FoodCardIcon fal fa-fw fa-" + icon}></i>
                <div className="FoodCardTitle">{food.name}</div>
                {
                    isPizza
                        ?
                        <div className="FoodCardPizzaPriceFlexbox">
                            <div className="FoodCardPizzaPriceContainer">
                                <span className="FoodCardPizzaSize">Kicsi</span>
                                <span className="FoodCardPizzaPrice">{food.smallPrice}</span>
                            </div>
                            <div className="FoodCardPizzaPriceContainer">
                                <span className="FoodCardPizzaSize">Normál</span>
                                <span className="FoodCardPizzaPrice">{food.price}</span>
                            </div>
                            <div className="FoodCardPizzaPriceContainer">
                                <span className="FoodCardPizzaSize">Nagy</span>
                                <span className="FoodCardPizzaPrice">{food.largePrice}</span>
                            </div>
                        </div>
                        :
                        <>
                            <div>
                                <span className="FoodCardSimplePrice">{food.price}</span>
                            </div>
                            {isMenu && !isToday
                                ?
                                <div className="FoodCardNotAvail">
                                    <span>Nem elérhető</span>
                                </div>
                                :
                                <button className="FoodCardSimpleBuyButton">
                                    <i className="FoodCardSimpleBuyIcon fal fa-cart-plus"></i>
                                </button>
                            }
                        </>
                }
                <Link to={"/rating/" + food.Id} className="FoodCardRatingButton">
                    Értékelés
                </Link>
            </>
        </div>
    );
}

export default FoodCard;
