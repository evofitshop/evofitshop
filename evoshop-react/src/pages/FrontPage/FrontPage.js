import React, { useEffect, useState } from 'react';
import Banner from './Banner';
import FoodList from './FoodList';
import Navbar from './Navbar';
import PlusFood from './PlusFood';
import SortFood from './SortFood';

function FrontPage(props) {
    const [foods, setFoods] = useState(props.foods);
    const [sortMethod, setSortMethod] = useState(0);
    const [selectedCategory, setSelectedCategory] = useState(-1);
    const [selectedPlusCategories, setSelectedPlusCategories] = useState([]);

    useEffect(() => {
        let f = [...props.foods];
        f = onCategorySelect(f, selectedCategory);
        f = onPlusCategorySelect(f, selectedPlusCategories);
        f = onSort(f, sortMethod);
        setFoods(f);
    }, [sortMethod, selectedCategory, selectedPlusCategories, props.foods]);

    function onSort(f, method) {
        switch (method) {
            case 1: // Név szerint
                f.sort((a, b) => a.name.localeCompare(b.name));
                break;
            case 2: // Legújabb
                f.sort((a, b) => b.addTime.localeCompare(a.addTime));
                break;
            case 3: // Legalacsonyabb ár
                f.sort((a, b) => a.price - b.price);
                break;
            case 4: // Legmagasabb ár
                f.sort((a, b) => b.price - a.price);
                break;
            default: // ID szerint / "személyreszabott"
                f.sort((a, b) => a.Id - b.Id);
                break;
        }
        return f;
    }

    function onCategorySelect(f, category) {
        if (category === 200) { // Heti menü
            f = f.filter(food => food.Id >= 34 && food.Id <= 40);
        } else if (category === 100) { // Húsos
            f = f.filter(food => food.FoodType === 0);
        } else if (category === 101) { // Vega
            f = f.filter(food => food.FoodType === 1);
        } else if (category < 100 && category >= 0) {
            f = f.filter(food => food.type === category);
        }
        return f;
    }

    function onPlusCategorySelect(f, plusTypes) {
        if (!plusTypes ||
            (plusTypes && plusTypes.every(c => c === false))) {
            return f;
        }

        plusTypes.forEach((type, i) => {
            if (type) {
                f = f.concat(props.foods.filter(f => f.type == i));
            }
        });

        f = [...new Set(f)];

        return f;
    }

    return (
        <div>
            <Navbar user={props.user} onLogout={props.onLogout} />
            <Banner callback={setSelectedCategory} />
            <div className="choose-food-container" id="choose-food-container">
                <PlusFood callback={setSelectedPlusCategories} />
                <SortFood callback={setSortMethod} />
                <FoodList foods={foods} />
            </div>
        </div>
    );
}

export default FrontPage;
