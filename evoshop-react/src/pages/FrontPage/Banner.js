import React from 'react';

function Banner({ callback }) {
    const mainCategories = [
        { id: 0, name: "Desszert" },
        { id: 1, name: "Gyros" },
        { id: 2, name: "Hamburger" },
        { id: 3, name: "Leves" },
        { id: 4, name: "Pizza" },
        { id: 5, name: "Saláta" },
        { id: 8, name: "Tészta" },
        { id: 11, name: "Köret" },
        { id: 12, name: "Szósz" },
        { id: 13, name: "Savanyúság" },
        { id: 100, name: "Húsos" },
        { id: 101, name: "Vegetáriánus" }
    ];

    return (
        <div className="banner-container">
            <h1 className="banner-title">Mit szeretnél enni?</h1>
            <ul className="menu-list" id="menu-list">
                {mainCategories.map(category => (
                    <li key={`menu-food-${category.id}`}
                        className="menu-food" onClick={callback.bind(this, category.id)}>
                        {category.name}
                    </li>
                ))}
            </ul>
            <button className="weekly-menu" id="weekly-menu" onClick={callback.bind(this, 200)}>Heti Menü</button>
        </div>
    );
}

export default Banner;
