document.getElementById("nav-back").addEventListener("click", (e) => {
    window.location.href = '../index.html';
  })



  if (getCookie("guid").length != 0) {
    const url = "https://localhost:44319/api/Minus";
  
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader('guid', getCookie("guid"));
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          var json = JSON.parse(xhr.responseText);
          console.log(json);
          document.getElementsByClassName("name")[0].textContent ="Felhasználónév: " + json.Name;
          document.getElementsByClassName("calorie")[0].textContent = "Kalóriamennyiség: " + json.calorie + " kcal";
  
        } else if (xhr.status == 400 || xhr.status == 500) {
          console.log(xhr.responseText);
        }
      }
    };
    var data = JSON.stringify({});
    xhr.send(data);
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
