var CART = [];

if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready();
}

// // Get user's cart
// const url = "https://localhost:44319/api/Cart/";
// var xhr = new XMLHttpRequest();
// xhr.open("GET", url, true);
// xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
// xhr.setRequestHeader("Content-Type", "application/json");
// xhr.setRequestHeader('guid', getCookie("guid"));
// xhr.onreadystatechange = function () {
//     if (xhr.readyState === 4) {
//         if (xhr.status === 200) {
//             var json = JSON.parse(xhr.responseText);
//             console.log(json);
//         } else if (xhr.status == 400 || xhr.status == 500) {
//             console.log(xhr.responseText);
//         }
//     }
// };


function addFoodToCart(fullId) {
    var fullId = fullId
    var size = fullId.split("-");
    
    var cartItem = CART.filter((x) => { return x.ProductId === parseInt(size[0]) && x.Size === parseInt(size[1]); });
    if (cartItem.length === 0) {
        CART.push({ 
            ProductId: parseInt(size[0]), 
            Size: parseInt(size[1]),
            Quantity: 1
        });
    }

    var id = size[0]
    size = size[1];
    var quantityValueElement = document.getElementsByClassName("quantity-value");
    if (quantityValueElement.length == 0) {
        drawCartItem(fullId)
    } else {
        for (var i = 0; i < quantityValueElement.length; i++) {
            let quantityValueElementFullId = quantityValueElement[i].getAttribute("id");
            quantityValueElementFullId = quantityValueElementFullId.split("-");
            let quantityValueElementId = quantityValueElementFullId[0];
            let quantityValueElementSize = quantityValueElementFullId[1];
            if (id == quantityValueElementId && quantityValueElementSize == size) {
                changeQuantityPlus(fullId);
                return;
            }
        }
        drawCartItem(fullId)
    }

    updateCartTotal();
    ready();
};

function drawCart(name, price, foodId) {
    var cartContainer = document.getElementById("cart-empty-item");
    var cartItemWrap = document.createElement("div");
    cartItemWrap.classList.add("cart-item");

    var splitted = foodId.split("-");
    cartItemWrap.dataset.id = splitted[0];
    cartItemWrap.dataset.size = splitted[1];

    cartContainer.appendChild(cartItemWrap);

    var cartItemName = document.createElement("div");
    cartItemName.classList.add("cart-item-name");
    cartItemName.innerHTML = name;
    cartItemWrap.appendChild(cartItemName);

    var cartItemModify = document.createElement("div");
    cartItemModify.classList.add("cart-item-modify");
    cartItemWrap.appendChild(cartItemModify);

    var cartItemDelete = document.createElement("div");
    cartItemDelete.classList.add("delete-icon");
    cartItemModify.appendChild(cartItemDelete);

    var itemQuantity = document.createElement("div");
    itemQuantity.classList.add("item-quantity");
    cartItemModify.appendChild(itemQuantity);

    var quantityValue = document.createElement("p");
    quantityValue.classList.add("quantity-value", "noselect");
    quantityValue.innerHTML = "1";
    quantityValue.setAttribute("id", `${foodId}`);
    itemQuantity.appendChild(quantityValue);

    var itemPlus = document.createElement("div");
    itemPlus.classList.add("plus", "noselect");
    itemPlus.innerHTML = "+";
    itemPlus.setAttribute("id", `${foodId}`);
    itemPlus.setAttribute("onClick", `changeQuantityPlus(this.id)`);
    itemQuantity.appendChild(itemPlus);

    var itemMinus = document.createElement("div");
    itemMinus.classList.add("minus", "noselect");
    itemMinus.innerHTML = "-";
    itemMinus.setAttribute("id", `${foodId}`);
    itemMinus.setAttribute("onClick", "changeQuantityMinus(this.id)");
    itemQuantity.appendChild(itemMinus);

    var itemPrice = document.createElement("div");
    itemPrice.classList.add("cart-item-price");
    itemPrice.innerHTML = price;
    cartItemModify.appendChild(itemPrice);

    var itemLine = document.createElement("div");
    itemLine.classList.add("wrapper");
    cartItemWrap.appendChild(itemLine);

    var itemLineInner = document.createElement("div");
    itemLineInner.classList.add("divider-cart", "div-transparent");
    itemLine.appendChild(itemLineInner);

    document.getElementById("shopping-cart-counter").innerHTML++;

    let amount = document.getElementById("cart-pay-amount").innerHTML;
    amount = parseInt(amount);
    document.getElementById("cart-pay-amount").innerHTML = amount += price;
}

function drawCartItem(fullId) {
    let localFoods = JSON.parse(localStorage.getItem('foods'));
    let id = fullId.split("-");
    let size = id[1];
    id = id[0];
    for (let index = 0; index < localFoods.length; index++) {
        if (localFoods[index].Id == id) {
            if (size == 3) {
                drawCart(`${localFoods[index].name}`, localFoods[index].price, fullId);
            } else if (size == 0) {
                drawCart(`${localFoods[index].name} (kicsi)`, localFoods[index].smallPrice, fullId);
            } else if (size == 1) {
                drawCart(`${localFoods[index].name} (normál)`, localFoods[index].price, fullId);
            } else {
                drawCart(`${localFoods[index].name} (nagy)`, localFoods[index].largePrice, fullId);
            }
        }
    }
}

function ready() {
    var deleteElement = document.getElementsByClassName("delete-icon");

    for (var i = 0; i < deleteElement.length; i++) {
        deleteElement[i].addEventListener('click', removeCartItem);
    }

    $(".pay-button").unbind().click(pay);
}

function pay() {
    if (!CART || CART.length === 0) {
        alert("Kérlek adj hozzá legalább 1 terméket a kosárhoz mielőtt fizetni szeretnél!");
        return;
    }

    localStorage.setItem("cart", JSON.stringify(CART));
    window.location.href = "/pay/pay.html";
}

function changeQuantityPlus(fullId) {
    fullId = fullId.split("-");
    let id = fullId[0];
    let size = fullId[1];

    var cartItem = CART.filter((x) => { return x.ProductId === parseInt(id) && x.Size === parseInt(size); });
    cartItem[0].Quantity++;

    var quantityValueElement = document.getElementsByClassName("quantity-value");

    for (var i = 0; i < quantityValueElement.length; i++) {
        let quantityValueElementId = quantityValueElement[i].id.split("-");
        let quantityValueElementSize = quantityValueElementId[1];
        quantityValueElementId = quantityValueElementId[0];
        if (quantityValueElementId == id && quantityValueElementSize == size) {

            var quantityValue = parseInt(quantityValueElement[i].innerText);
            quantityValueElement[i].innerText = quantityValue + 1;
            break;
        }
    }
    updateCartTotal()
}

function changeQuantityMinus(fullId) {
    fullId = fullId.split("-");
    let id = fullId[0];
    let size = fullId[1];

    var cartItem = CART.filter((x) => { return x.ProductId === parseInt(id) && x.Size === parseInt(size); });
    if (cartItem[0].Quantity > 1) {
        cartItem[0].Quantity--;
    }

    var quantityValueElement = document.getElementsByClassName("quantity-value");

    for (var i = 0; i < quantityValueElement.length; i++) {
        let quantityValueElementId = quantityValueElement[i].id.split("-");
        let quantityValueElementSize = quantityValueElementId[1];
        quantityValueElementId = quantityValueElementId[0];
        if (quantityValueElementId == id && quantityValueElementSize == size) {
            var quantityValue = parseInt(quantityValueElement[i].innerText);
            if ((quantityValue - 1) == 0) {
                alert("A mennyiség nem lehet egytől kevesebb!")
            } else {
                quantityValueElement[i].innerText = quantityValue - 1;
            }
            break;
        }
    }
    updateCartTotal()
}

function removeCartItem(event) {
    var buttonClicked = event.target;
    var item = buttonClicked.parentElement.parentElement;

    CART = CART.filter((x) => { return x.ProductId !== parseInt(item.dataset.id) || x.Size !== parseInt(item.dataset.size); })

    item.remove();
    document.getElementById("shopping-cart-counter").innerHTML--;
    updateCartTotal();
}

function updateCartTotal() {
    var total = 0

    var quantityValueElement = document.getElementsByClassName("quantity-value");
    var priceElement = document.getElementsByClassName('cart-item-price');



    for (var i = 0; i < quantityValueElement.length; i++) {
        var price = parseInt(priceElement[i].innerText)
        var quantity = parseInt(quantityValueElement[i].innerText)
        total += price * quantity;
    }

    document.getElementById('cart-pay-amount').innerText = total;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Item flys to cart
function foodFlysToCart() {

    setTimeout(function () {
        $('.buy-item').on('click', function () {
            var cart = $('.cart-buttom');
            var imgtodrag = $(this).parent('.dynamic-food').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');

                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }
        });

        $('.pizza-price').on('click', function () {
            var cart = $('.cart-buttom');
            var imgtodrag = $(this).parent('.button-wrap').parent('.all-buttons-wrap').parent('.dynamic-pizza').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');

                // setTimeout(function () {
                //     cart.effect("shake", {
                //         times: 2
                //     }, 200);
                // }, 1500);

                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }
        });
    }, 1000);
    jQuery(function ($) {
        $(window).scroll(function fix_element() {
            $('.cart-wrap').css(
                $(window).scrollTop() > 650 ? {
                    'position': 'fixed',
                    'top': '10px',
                    'border-radius': '40px',
                    'right': '-130px',
                    'background-color': '#a6c47c',
                    'width': '180px'
                } : {
                    'position': 'relative',
                    'top': 'auto',
                    'right': '0',
                    'background-color': '',
                    'height': '50px',
                    'width': '50px'
                }
            );
            $('.cart-items-wrap').css(
                $(window).scrollTop() > 650 ? {
                    'right': '130px'
                } : {
                    'right': '0'
                }
            );
            return fix_element;
        }());
    });
}

foodFlysToCart();