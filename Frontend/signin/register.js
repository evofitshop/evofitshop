document.getElementById("signin-button-send").addEventListener("click",(e)=> {
    var inputName = document.getElementById("name").value.trim();
    var inputPhone = document.getElementById("phone").value.trim();
    var inputEmail = document.getElementById("email").value.trim();
    var inputPwd = document.getElementById("password").value.trim();
    var inputZip = document.getElementById("zip").value.trim();
    var inputCity = document.getElementById("city").value.trim();
    var inputStreet = document.getElementById("street").value.trim();
    var inputStreetNumber = document.getElementById("streetNumber").value.trim();
    var inputFloorAndDoor = document.getElementById("FloorAndDoor").value.trim();
    var inputBell = document.getElementById("bell").value;


    console.log(inputPhone)
    const url = "https://localhost:44319/api/Session/Register";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin","*");
    xhr.setRequestHeader("Content-Type","application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var json = JSON.parse(xhr.responseText);
                console.log(json);
                setCookie("guid", json, 30);
                window.location.href = '../index.html';
            }
            else if (xhr.status == 400 || xhr.status == 500) {
                let errorMsg = xhr.responseText;
                document.getElementById("backend-error").innerHTML = errorMsg.substring(1, errorMsg.length-1);
                document.getElementById("backend-error").style.display = "block";
            }
        }
    };
    
    // if(mustFill() & validateEmail(inputEmail) & checkPassword(inputPwd) & checkPhone(inputPhone) & checkZip(inputZip))
    if(mustFill() & validateEmail(inputEmail) & checkPassword(inputPwd) & checkPhone(inputPhone) & checkZip(inputZip)) {
        document.getElementById("invalid-email").style.display = "none";
        document.getElementById("invalid-pwd").style.display = "none";
        document.getElementById("invalid-phone").style.display = "none";
        document.getElementById("invalid-zip").style.display = "none";
        document.getElementById("backend-error").style.display = "none";
        var data = JSON.stringify({
            'EmailAddress': `${inputEmail}`,
            'Password': `${inputPwd}`,
            'Name': `${inputName}`,
            'PhoneNumber': `${inputPhone}`,
            'Address':
            {
                'City': `${inputCity}`,
                'Street': `${inputStreet}`,
                'HouseNumber': `${inputStreetNumber}`,
                'FloorAndDoor': `${inputFloorAndDoor}`,
                'ZipCode': `${inputZip}`,
                'DoorBell': `${inputBell}`
            }
        });
        xhr.send(data);
    } else {

    }
})

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function validateEmail(inputEmail) 
{
    var inputEmailLength = document.getElementById("email").value.trim().length;

    if(inputEmailLength > 0) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(inputEmail))
        {
            return (true)
        } else {
            document.getElementById("invalid-email").style.display = "block";
            return (false)
        }
    }
}

function checkPassword(pwd){
    // at least one number, one lowercase and one uppercase letter
    // at least six characters

    var inputPwd = document.getElementById("password").value.trim().length;

    if(inputPwd > 0) {
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
        if (re.test(pwd))
        {
            return (true)
        } else {
            document.getElementById("invalid-pwd").style.display = "block";
            return (false)
        }
    }
}

function checkPhone(phone){
    // +36203176914
    var inputPhone = document.getElementById("phone").value.trim().length;

    if(inputPhone > 0) {
        var re = /^\+36(?:(?:(?:1|20|30|31|50|70)[1-9]\d{6})|[1-9]\d{7})$/;
        if (re.test(phone))
        {
            return (true)
        } else {
            document.getElementById("invalid-phone").style.display = "block";
            return (false)
        }
    }
}

function checkZip(zip){
    var inputZip = document.getElementById("zip").value.trim().length;

    if(inputZip > 0) {
        if (zip >= 1000 && zip <= 9999)
        {
            return (true)
        } else {
            document.getElementById("invalid-zip").style.display = "block";
            return (false)
        }
    }
}

function mustFill() {
    var inputName = document.getElementById("name").value.trim().length;
    var inputPhone = document.getElementById("phone").value.trim().length;
    var inputEmail = document.getElementById("email").value.trim().length;
    var inputPwd = document.getElementById("password").value.trim().length;
    var inputZip = document.getElementById("zip").value.trim().length;
    var inputCity = document.getElementById("city").value.trim().length;
    var inputStreet = document.getElementById("street").value.trim().length;
    var inputStreetNumber = document.getElementById("streetNumber").value.trim().length;

    if(checkFilledN() & checkFilledP() & checkFilledE() & checkFilledPwd() & checkFilledZ() & checkFilledC() & checkFilledS() & checkFilledSN()) {
        return true;
    } else {
        return false;
    }

    function checkFilledN () {
        if(inputName === 0) {
            document.getElementById("empty-name").style.display = "block";
            document.getElementById("empty-name").innerHTML = "A név mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-name").style.display = "none";
            return true;
        }
    }

    function checkFilledP () {
        if(inputPhone === 0) {
            document.getElementById("empty-phone").style.display = "block";
            document.getElementById("empty-phone").innerHTML = "A telefonszám mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-phone").style.display = "none";
            return true;
        }
    }

    function checkFilledE () {
        if(inputEmail === 0) {
            document.getElementById("empty-email").style.display = "block";
            document.getElementById("empty-email").innerHTML = "Az e-mail mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-email").style.display = "none";
            return true;
        }
    }

    function checkFilledPwd () {
        if(inputPwd === 0) {
            document.getElementById("empty-pwd").style.display = "block";
            document.getElementById("empty-pwd").innerHTML = "A jelszó mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-pwd").style.display = "none";
            return true;
        }
    }

    function checkFilledZ () {
        if(inputZip === 0) {
            document.getElementById("empty-zip").style.display = "block";
            document.getElementById("empty-zip").innerHTML = "Az irányítószám mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-zip").style.display = "none";
            return true;
        }
    }

    function checkFilledC () {
        if(inputCity === 0) {
            document.getElementById("empty-city").style.display = "block";
            document.getElementById("empty-city").innerHTML = "A település mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-city").style.display = "none";
            return true;
        }
    }

    function checkFilledS () {
        if(inputStreet === 0) {
            document.getElementById("empty-street").style.display = "block";
            document.getElementById("empty-street").innerHTML = "Az utca mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-street").style.display = "none";
        return true;
        }
    }

    function checkFilledSN () {
        if(inputStreetNumber === 0) {
            document.getElementById("empty-streetNumber").style.display = "block";
            document.getElementById("empty-streetNumber").innerHTML = "A házszám mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-streetNumber").style.display = "none";
            return true;
        }
    }
}