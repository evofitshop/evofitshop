// const cartItems = document.getElementById("cart-items");
const loginButton = document.getElementsByClassName("login-button");
const signButton = document.getElementsByClassName("signin-button");
const welcome = document.getElementsByClassName("welcome-user");
const calorie = document.getElementsByClassName("calorie-button");

$(function() {
  $('#cart-items').hide().click(function(e) {
    e.stopPropagation();
  });      
  $("div.cart-img").click(function(e) {
    $('#cart-items').animate({ opacity: "toggle" },200);
    e.stopPropagation();
  });
  $(document).click(function() {
    $('#cart-items').fadeOut("fast");
  });
});

// If you have a cookie log in
if(getCookie("guid").length != 0) {
  const url = "https://localhost:44319/api/User";

  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.setRequestHeader("Access-Control-Allow-Origin","*");
  xhr.setRequestHeader("Content-Type","application/json");
  xhr.setRequestHeader('guid',getCookie("guid"));
  xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
          if (xhr.status === 200) {
              var json = JSON.parse(xhr.responseText);
              document.getElementsByClassName("logged-in")[0].style.display = "flex";
              document.getElementsByClassName("login-button")[0].style.display = "none";
              document.getElementsByClassName("signin-button")[0].style.display = "none";
              let p = document.createElement("p");
              var node = document.createTextNode(`Üdv, ${json.Name}`);
              p.appendChild(node);
              document.getElementsByClassName("welcome-user")[0].appendChild(p);
          }
          else if (xhr.status == 400 || xhr.status == 500) {
              console.log(xhr.responseText);
          }
      }
  };
  var data = JSON.stringify(
      {
      }
  );
  xhr.send(data);
}

// Log out
document.getElementById("user-profile-logout").addEventListener("click",(e)=> {
  const url = "https://localhost:44319/api/Session/Logout";
  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Access-Control-Allow-Origin","*");
  xhr.setRequestHeader("Content-Type","application/json");
  xhr.setRequestHeader('guid',getCookie("guid"));
  xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
          if (xhr.status === 200) {
              document.cookie = "guid" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
              document.getElementsByClassName("logged-in")[0].style.display = "none";
              document.getElementsByClassName("login-button")[0].style.display = "block";
              document.getElementsByClassName("signin-button")[0].style.display = "block";
          }
          else if (xhr.status == 400 || xhr.status == 500) {
              console.log(xhr.responseText);
          }
      }
  };
  var data = JSON.stringify(
      {
      }
  );
  xhr.send(data);
})


function checkCookie() {
  var username = getCookie("guid");
  if (username != "") {
    console.log("Welcome again " + username);
  } else {
    console.log("no cookie");
  }
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

//minuscalorie
document.getElementsByClassName("login-button pay-button").addEventListener("click",(e) =>{

  
})