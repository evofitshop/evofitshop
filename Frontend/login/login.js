document.getElementById("login-button-send").addEventListener("click",(e)=> {
    var inputEmail = document.getElementById("email").value.trim();
    var inputPwd = document.getElementById("password").value.trim();
    const url = "https://localhost:44319/api/Session/Login";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin","*");
    xhr.setRequestHeader("Content-Type","application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var json = JSON.parse(xhr.responseText);
                console.log(json);
                setCookie("guid", json, 30);
                window.location.href = '../index.html';
            }
            else if (xhr.status == 400 || xhr.status == 500) {
                let errorMsg = xhr.responseText;
                document.getElementById("backend-error").innerHTML = errorMsg.substring(1, errorMsg.length-1);
                document.getElementById("backend-error").style.display = "block";
            }
        }
    };

    if(mustFill()) {
        var data = JSON.stringify(
            {
                'EmailAddress': `${inputEmail}`,
                'Password': `${inputPwd}`
            }
        );
        xhr.send(data);
    } else {

    }
    
})

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function mustFill() {
    var inputEmail = document.getElementById("email").value.trim().length;
    var inputPwd = document.getElementById("password").value.trim().length;

    if(checkFilledEmail() & checkFilledPwd()) {
        return true;
    } else {
        return false;
    }

    function checkFilledEmail() {
        if(inputEmail === 0) {
            document.getElementById("empty-email").style.display = "block";
            document.getElementById("empty-email").innerHTML = "Az email mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-email").style.display = "none";
            return true;
        }
    }

    function checkFilledPwd() {
        if(inputPwd === 0) {
            document.getElementById("empty-pwd").style.display = "block";
            document.getElementById("empty-pwd").innerHTML = "A jelszó mező kitöltése kötelező!";
            return false;
        } else {
            document.getElementById("empty-pwd").style.display = "none";
            return true;
        }
    }
}