// Get food
function httpGetAsync(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}


httpGetAsync("https://localhost:44319/api/Food/Get", function (arr) {
    let result = JSON.parse(arr);
    localStorage.setItem('foods', JSON.stringify(result));
    result = JSON.parse(localStorage.getItem('foods'));

    for (let index = 0; index < result.length; index++) {
        if (result[index].type == 4 && (result[index].Id < 34 ||  result[index].Id > 40)) {
            drawPizza(result, index);
        } else if((result[index].Id < 34 ||  result[index].Id > 40)) {
            drawFood(result, index);
        }
    }
})

/* Heti menu */
{
var levesek = [];
var foetel = [];
var desszert = [];
document.getElementById("weekly-menu").addEventListener("click", function (e){
    let foodsArray = JSON.parse(localStorage.getItem('foods'));
    removeFoods();
    var today = new Date();
    var db = foodsArray[33].addTime;
    var valami = db.substring(0,10);
    var parts = valami.split('-');
    var mydate = new Date(parts[0],parts[1]-1,parts[2]);
    var akarmi =  Math.floor((today - mydate)/(24*60*60*1000))
    var day = 33;
    for(let i = 0; i <= 6; i++)
    {
    if(Math.floor((today - mydate)/(24*60*60*1000)) == i)
    {
        break;
    }
    else{
        day++;
    }  
 }
    if(day>33)
    {
        for(let i = 33; i < day; i++)
        {
            drawNotFood(foodsArray, i);
        }
        for(let i = day; i < 40; i++){
            drawFood(foodsArray, i);
        }
    }
    else if(day == 33){
        for(let i = day; i < 40; i++){
            drawFood(foodsArray, i);
        }
    }
    
   
})
}

// Choose food from menu
document.getElementById("menu-list").addEventListener("click", function (e) {
    if (e.target && e.target.matches("li.menu-food")) {
        let foodsArray = JSON.parse(localStorage.getItem('foods'));
        if (e.target.innerHTML === "Desszert") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 0 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Gyros") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 1 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Hamburger") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 2 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Leves") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 3 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Pizza") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 4 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawPizza(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Saláta") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 5 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Tészta") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 8 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Húsos") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].FoodType == 0 && foodsArray[index].type == 4 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawPizza(foodsArray, index);
                }
                else if (foodsArray[index].FoodType == 0 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)){
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Vegetáriánus") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].FoodType == 1 && foodsArray[index].type == 4 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawPizza(foodsArray, index);
                }
                else if (foodsArray[index].FoodType == 1 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
            }
        } else if (e.target.innerHTML === "Köret") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 11 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
           }
        }
        else if (e.target.innerHTML === "Szósz") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 12 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
           }
        }
        else if (e.target.innerHTML === "Savanyúság") {
            removeFoods();

            for (let index = 0; index < foodsArray.length; index++) {
                if (foodsArray[index].type == 13 && (foodsArray[index].Id < 34 ||  foodsArray[index].Id > 40)) {
                    drawFood(foodsArray, index);
                }
           }
        }
    }
});

// Add plus food
var selections = {};
var checkboxElems = document.querySelectorAll("input[type='checkbox']");

for (var i = 0; i < checkboxElems.length; i++) {
  checkboxElems[i].addEventListener("change", displayCheck);
}

function displayCheck(e) {
  if (e.target.checked) {
    selections[e.target.id] = {
      value: e.target.value
    };
  } 
  else {
    delete selections[e.target.id];
    removeElementsByClass(`value-${e.target.value}`);
  }

  var result = [];
  var total = 0;

  for (var key in selections) {
    let foods = JSON.parse(localStorage.getItem('foods'));
    if(selections[key].value == 0) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 6) {
                drawPlusFood(foods, index, 0);
            }
        }
    }
    else if(selections[key].value == 1) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 7) {
                drawPlusFood(foods, index, 1);
            }
        }
    }
    else if(selections[key].value == 2) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 9) {
                drawPlusFood(foods, index, 2);
            }
        }
    }
    else if(selections[key].value == 3) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 10) {
                drawPlusFood(foods, index, 3);
            }
        }
    }
    else if(selections[key].value == 4) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 11) {
                drawPlusFood(foods, index, 4);
            }
        }
    }
    else if(selections[key].value == 5) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 12) {
                drawPlusFood(foods, index, 5);
            }
        }
    }
    else if(selections[key].value == 6) {
        for (let index = 0; index < foods.length; index++) {
            if(foods[index].type == 13) {
                drawPlusFood(foods, index, 6);
            }
        }
    }
  }
}

// Sort food
document.getElementById('search_categories').addEventListener('change', (e) => {
    let tempFoods = [];
    let foods = document.getElementsByClassName("food-title");
    let localFoods = JSON.parse(localStorage.getItem('foods'));
    for (let i = 0; i < foods.length; i++) {
        for (let j = 0; j < localFoods.length; j++) {
            if(foods[i].textContent == localFoods[j].name) {
                // console.log(localFoods[j].name)
                tempFoods.push(localFoods[j]);
            }
        }
    }
    let selected = document.getElementById('search_categories');
    let selectedValue = selected.options[selected.selectedIndex].value;
    if (selectedValue == 1) {
        tempFoods.sort(function (a, b) {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }
            return 0;
        })
        removeFoods();
        for (let index = 0; index < tempFoods.length; index++) {
            if(tempFoods[index].type == 4) {
                drawPizza(tempFoods, index);
            }
            else {
                drawFood(tempFoods, index);
            }
        }
    }
    if (selectedValue == 2) {
        tempFoods.sort(function (a, b) {
            if (a.addTime > b.addTime) {
                return -1;
            }
            if (a.addTime < b.addTime) {
                return 1;
            }
            return 0;
        })
        removeFoods();
        for (let index = 0; index < tempFoods.length; index++) {
            if(tempFoods[index].type == 4) {
                drawPizza(tempFoods, index);
            }
            else {
                drawFood(tempFoods, index);
            }
        }
    }
    if (selectedValue == 3) {
        tempFoods.sort(function (a, b) {
            if (a.price < b.price) {
                return -1;
            }
            if (a.price > b.price) {
                return 1;
            }
            return 0;
        })
        removeFoods();
        for (let index = 0; index < tempFoods.length; index++) {
            if(tempFoods[index].type == 4) {
                drawPizza(tempFoods, index);
            }
            else {
                drawFood(tempFoods, index);
            }
        }
    }
    if (selectedValue == 4) {
        tempFoods.sort(function (a, b) {
            if (a.price > b.price) {
                return -1;
            }
            if (a.price < b.price) {
                return 1;
            }
            return 0;
        })
        removeFoods();
        for (let index = 0; index < tempFoods.length; index++) {
            if(tempFoods[index].type == 4) {
                drawPizza(tempFoods, index);
            }
            else {
                drawFood(tempFoods, index);
            }
        }
    }
    
});

function drawPizza(pizzaArray, index) {

    var foodContainer = document.getElementById("food-container");
    var div = document.createElement("div");
    div.classList.add("dynamic-pizza");
    foodContainer.appendChild(div);

    var img = document.createElement("img");
    img.classList.add("food-item-img");
    img.src = "/img/pizzaItem.png"
    div.appendChild(img);

    var title = document.createElement("p");
    title.textContent = pizzaArray[index].name;
    title.classList.add("pizza-title");
    div.appendChild(title);

    var allButtonsWrap = document.createElement("div");
    allButtonsWrap.classList.add("all-buttons-wrap");
    div.appendChild(allButtonsWrap);

    // Kicsi
    var buttonWrapOne = document.createElement("div");
    buttonWrapOne.classList.add("button-wrap");
    allButtonsWrap.appendChild(buttonWrapOne);

    var buttonOneSizeName = document.createElement("p");
    buttonOneSizeName.textContent = "Kicsi";
    buttonOneSizeName.classList.add("size-name");
    buttonWrapOne.appendChild(buttonOneSizeName);

    var buttonOne = document.createElement("button");
    buttonOne.textContent = pizzaArray[index].smallPrice;
    buttonOne.classList.add("pizza-price");
    buttonOne.setAttribute("id", `${pizzaArray[index].Id}-0`); 
    buttonOne.setAttribute("onClick", `addFoodToCart(this.id)`); 
    buttonWrapOne.appendChild(buttonOne);


    // Közepes
    var buttonWrapTwo = document.createElement("div");
    buttonWrapTwo.classList.add("button-wrap");
    allButtonsWrap.appendChild(buttonWrapTwo);

    var buttonTwoSizeName = document.createElement("p");
    buttonTwoSizeName.textContent = "Normál";
    buttonTwoSizeName.classList.add("size-name");
    buttonWrapTwo.appendChild(buttonTwoSizeName);

    var buttonTwo = document.createElement("button");
    buttonTwo.textContent = pizzaArray[index].price;
    buttonTwo.classList.add("pizza-price");
    buttonTwo.setAttribute("id", `${pizzaArray[index].Id}-1`); 
    buttonTwo.setAttribute("onClick", `addFoodToCart(this.id)`); 
    buttonWrapTwo.appendChild(buttonTwo);

    // Nagy
    var buttonWrapThree = document.createElement("div");
    buttonWrapThree.classList.add("button-wrap");
    allButtonsWrap.appendChild(buttonWrapThree);

    var buttonThreeSizeName = document.createElement("p");
    buttonThreeSizeName.textContent = "Nagy";
    buttonThreeSizeName.classList.add("size-name");
    buttonWrapThree.appendChild(buttonThreeSizeName);

    var buttonThree = document.createElement("button");
    buttonThree.textContent = pizzaArray[index].largePrice;
    buttonThree.classList.add("pizza-price");
    buttonThree.setAttribute("id", `${pizzaArray[index].Id}-2`);
    buttonThree.setAttribute("onClick", `addFoodToCart(this.id)`); 
    buttonWrapThree.appendChild(buttonThree);
    

    // info
    var ToppingInfo = document.createElement("div");
    ToppingInfo.classList.add("Ptooltip");
    div.appendChild(ToppingInfo);

    var ToppingText = document.createElement("span");
    ToppingText.textContent = pizzaArray[index].topping + '\n' + pizzaArray[index].calorie + 'kcal';
    ToppingText.classList.add("Ptooltiptext");
    ToppingInfo.appendChild(ToppingText);

     // Rating
     var buttonWrapFour = document.createElement("div");
     buttonWrapFour.classList.add("button-pizzaRating");
     div.appendChild(buttonWrapFour);
 
     var rating_button = document.createElement("button");
     rating_button.textContent = "Hozzászólások";
     rating_button.classList.add("button-pizzaRating");
     rating_button.setAttribute("id", `${pizzaArray[index].Id}`);
     rating_button.setAttribute("onClick", `openWindow(this.id)`); 
     buttonWrapFour.appendChild(rating_button);

     var buttonWrapNewRat = document.createElement("div");
     buttonWrapNewRat.classList.add("button-pizzaNewRating");
     div.appendChild(buttonWrapNewRat);
 
     var newrating_button = document.createElement("button");
     newrating_button.textContent = "Új hozzászólás";
     newrating_button.classList.add("button-pizzaNewRating");
     newrating_button.setAttribute("id", `${pizzaArray[index].Id}`);
     newrating_button.setAttribute("onClick", `newRat(this.id)`); 
     buttonWrapNewRat.appendChild(newrating_button);


    newFood(div, pizzaArray, index);
    foodFlysToCart();
}


function drawFood(foodArray, index) {
    var foodContainer = document.getElementById("food-container");
    var div = document.createElement("div");
    div.classList.add("dynamic-food");
    foodContainer.appendChild(div);

    var img = document.createElement("img");
    img.classList.add("food-item-img");
    img.src = "/img/foodItem.png"
    div.appendChild(img);

    var title = document.createElement("p");
    title.textContent = foodArray[index].name;
    title.classList.add("food-title");
    div.appendChild(title);

    var price = document.createElement("p");
    price.textContent = foodArray[index].price;
    price.classList.add("food-price");
    div.appendChild(price);

    var cart = document.createElement("div");
    cart.classList.add("buy-item");
    cart.setAttribute("id", `${foodArray[index].Id}-1`);
    cart.setAttribute("onClick", `addFoodToCart(this.id)`); 
    div.appendChild(cart);

    // info
    var ToppingInfo = document.createElement("div");
    ToppingInfo.classList.add("tooltip");
    div.appendChild(ToppingInfo);

    var ToppingText = document.createElement("span");
    ToppingText.textContent = foodArray[index].topping + '\n' + foodArray[index].calorie  + 'kcal';
    ToppingText.classList.add("tooltiptext");
    ToppingInfo.appendChild(ToppingText);

     // Rating
     var buttonWrapFour = document.createElement("div");
     buttonWrapFour.classList.add("button-foodRating");
     div.appendChild(buttonWrapFour);
 
     var rating_button = document.createElement("button");
     rating_button.textContent = "Hozzászólások";
     rating_button.classList.add("button-foodRating");
     rating_button.setAttribute("id", `${foodArray[index].Id}`);
     rating_button.setAttribute("onClick", `openWindow(this.id)`); 
     buttonWrapFour.appendChild(rating_button);
 
     var buttonWrapNewRat = document.createElement("div");
     buttonWrapNewRat.classList.add("button-foodNewRating");
     div.appendChild(buttonWrapNewRat);
 
     var newrating_button = document.createElement("button");
     newrating_button.textContent = "Új hozzászólás";
     newrating_button.classList.add("button-foodNewRating");
     newrating_button.setAttribute("id", `${foodArray[index].Id}`);
     newrating_button.setAttribute("onClick", `newRat(this.id)`); 
     buttonWrapNewRat.appendChild(newrating_button);

    newFood(div, foodArray, index);
    foodFlysToCart();
}

function drawPlusFood(foodArray, index, foodValue) {
    var foodContainer = document.getElementById("food-container");
    var div = document.createElement("div");
    div.classList.add('food-wrap', `value-${foodValue}`, "dynamic-food");
    foodContainer.appendChild(div);

    var img = document.createElement("img");
    img.classList.add("food-item-img");
    img.src = "/img/foodItem.png"
    div.appendChild(img);

    var title = document.createElement("p");
    title.textContent = foodArray[index].name;
    title.classList.add("food-title");
    div.appendChild(title);

    var price = document.createElement("p");
    price.textContent = foodArray[index].price;
    price.classList.add("food-price");
    div.appendChild(price);

    var cart = document.createElement("div");
    cart.classList.add("buy-item");
    cart.setAttribute("id", `${foodArray[index].Id}-1`);
    cart.setAttribute("onClick", `addFoodToCart(this.id)`); 
    div.appendChild(cart);

    // info
    var ToppingInfo = document.createElement("div");
    ToppingInfo.classList.add("tooltip");
    div.appendChild(ToppingInfo);

    var ToppingText = document.createElement("span");
    ToppingText.textContent = foodArray[index].topping + '\n' + foodArray[index].calorie + 'kcal';
    ToppingText.classList.add("tooltiptext");
    ToppingInfo.appendChild(ToppingText);

    newFood(div, foodArray, index);
    foodFlysToCart();
}

function newFood(currentFood, foodArray, index) {
    var date = new Date(Date.parse(foodArray[index].addTime));
    var msInDay = 24 * 60 * 60 * 1000;
    date.setHours(0, 0, 0, 0);
    var now = new Date();
    now.setHours(0, 0, 0, 0);
    var diff = (+now - +date) / msInDay

    // max 14 napja lett felvéve
    if (diff <= 14) {
        var newBadge = document.createElement("div");
        newBadge.classList.add("new-product-badge");
        currentFood.appendChild(newBadge);
    }
}

function removeElement(id) {
    var elem = document.getElementById(id);
    return elem.parentNode.removeChild(elem);
}

function removeFoods() {
    var foodContainer = document.getElementById("food-container");
    foodContainer.parentNode.removeChild(foodContainer);

    var chooseFoodContainer = document.getElementById("choose-food-container");
    var div = document.createElement("div");
    div.classList.add("foods-container");
    div.setAttribute("id", "food-container");
    chooseFoodContainer.appendChild(div);
}

function removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
}

// Item flys to cart
function foodFlysToCart() {

    setTimeout(function () {
        $('.buy-item').on('click', function () {
            var cart = $('.cart-buttom');
            var imgtodrag = $(this).parent('.dynamic-food').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');

                setTimeout(function () {
                    cart.effect("shake", {
                        times: 2
                    }, 200);
                }, 1500);

                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }
        });

        $('.pizza-price').on('click', function () {
            var cart = $('.cart-buttom');
            var imgtodrag = $(this).parent('.button-wrap').parent('.all-buttons-wrap').parent('.dynamic-pizza').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');

                // setTimeout(function () {
                //     cart.effect("shake", {
                //         times: 2
                //     }, 200);
                // }, 1500);

                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }
        });
    }, 1000);

    jQuery(function ($) {
        $(window).scroll(function fix_element() {
            $('.cart-wrap').css(
                $(window).scrollTop() > 700 ?
                {
                    'position': 'fixed',
                    'top': '10px',
                    'border-radius': '40px',
                    'right': '-130px',
                    'background-color': 'rgb(107, 182, 130)',
                    'width': '80px'
                } :
                {
                    'position': 'relative',
                    'top': 'auto',
                    'right': '0',
                    'background-color': '',
                    'height': '50px',
                    'width': '50px'
                }
            );
            $('.cart-items-wrap').css(
                $(window).scrollTop() > 650 ?
                {
                    'right': '130px'
                } :
                {
                    'right': '0'
                }
            );
            return fix_element;
        }());
    });
}

function drawNotFood(foodArray, index) {
    var foodContainer = document.getElementById("food-container");
    var div = document.createElement("div");
    div.classList.add("dynamic-notMenu");
    foodContainer.appendChild(div);

    var img = document.createElement("img");
    img.classList.add("food-item-img");
    img.src = "/img/foodItem.png"
    div.appendChild(img);

    var title = document.createElement("p");
    title.textContent = foodArray[index].name;
    title.classList.add("food-title");
    div.appendChild(title);

    var price = document.createElement("p");
    price.textContent = foodArray[index].price;
    price.classList.add("food-price");
    div.appendChild(price);


    // var imgCart = document.createElement("img")
    // imgCart.classList.add("buy-item");
    // cart.appendChild(imgCart)

    // info
    var ToppingInfo = document.createElement("div");
    ToppingInfo.classList.add("tooltip");
    div.appendChild(ToppingInfo);

    var ToppingText = document.createElement("span");
    ToppingText.textContent = foodArray[index].topping;
    ToppingText.classList.add("tooltiptext");
    ToppingInfo.appendChild(ToppingText);


    newFood(div, foodArray, index);
}