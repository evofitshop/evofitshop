"use strict";

var CART = [];
var FOODS = [];
var USER = {};
const FOOD_SIZE = [
    "kicsi",
    "közepes",
    "nagy"
];
const USER_GUID = getCookie("guid");
var confirmModal = {};
var alertModal = {};

$(document).ready(() => {
    $(".time").html(Math.floor(getRandom(30, 180)));

    confirmModal = $('[data-remodal-id=modal-confirm]').remodal();
    alertModal = $('[data-remodal-id=modal-alert]').remodal();

    if (USER_GUID.length == 0) {
        alert("a GUID süti nem található!");
        window.location.href = "/index.html";
    }

    var cartJson = localStorage.getItem("cart");
    var foodJson = localStorage.getItem("foods");
    if (!cartJson || !foodJson) {
        alert("Kérlek adj hozzá legalább 1 terméket a kosárhoz mielőtt fizetni szeretnél!");
        window.location.href = "/index.html";
    }

    CART = JSON.parse(cartJson);
    FOODS = JSON.parse(foodJson);

    USER = getUser();
    console.log(USER);
    loadAddress(USER.Addresses[0]);

    render();
});

function render() {
    var total = 0;
    const $table = $("table");

    $("tr.added").remove();

    $.each(CART, (i, item) => {
        const food = FOODS[item.ProductId - 1];
        var price = 0;
        switch (item.Size) {
            case 0:
                price = food.smallPrice;
                break;
            case 1:
                price = food.price;
                break;
            case 2:
                price = food.largePrice;
                break;
        }

        total += item.Quantity * price;

        $table.append(`
      <tr class="added">
        <td>${food.name} ${food.type === 4 ? `(${FOOD_SIZE[item.Size]})` : ""}</td>
        <td>${price} Ft</td>
        <td>${item.Quantity} db</td>
        <td>${item.Quantity * price} Ft</td>
        <td>
          <div class="modify">
            <i class="bi bi-plus-circle" onclick="plus(${i})"></i>
            <i class="bi bi-dash-circle" onclick="minus(${i})"></i>
            <i class="bi bi-x-circle" onclick="del(${i})"></i>
          </div>
        </td>
      </tr>
    `);
    });

    $(".total").html(total);
}

function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

function plus(i) {
    CART[i].Quantity++;
    render();
}

function minus(i) {
    if (CART[i].Quantity > 1) {
        CART[i].Quantity--;
        render();
    } else {
        alert("A mennyiség nem lehet 1-től kevesebb!");
    }
}

function del(i) {
    CART = CART.filter((x) => { return x !== CART[i]; });
    render();
}

function back() {
    $(".modal-confirm #modal-content").html("Szeretnéd törölni a kosár tartalmát?");
    confirmModal.open();

    $(document).on('confirmation', '.remodal.modal-confirm', function () {
        window.location.href = "/index.html";
    });
}

function submit() {
    $(".modal-confirm #modal-content").html("Szeretnéd leadni a megrendelést?");
    confirmModal.open();

    $(document).on('confirmation', '.remodal.modal-confirm', function () {
        var order =
        {
            Cart: CART,
            Message: $("textarea").val(),
            AddressId: USER.Addresses[0].Id
        }

        console.log(order);

        const submittedOrder = $.post({
            url: "https://localhost:44319/api/Order",
            headers: {
                guid: USER_GUID,
            },
            contentType: "application/json",
            data: JSON.stringify(order),
            async: false
        });

        console.log(submittedOrder);

        if (submittedOrder.status == 200) {
            window.location.href = "/pay/finish/finish.html";
        } else {
            $(".modal-alert #modal-content").html("A megrendelést nem sikerült feladni szerverhiba miatt!");
            alertModal.open();
        }
    });
}

function getUser() {
    const user = $.get({
        url: "https://localhost:44319/api/User",
        headers: {
            guid: USER_GUID
        },
        async: false
    });

    return user.responseJSON;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function loadAddress(a) {
    $(".address").html(`
        ${a.ZipCode} ${a.City}, ${a.Street} ${a.HouseNumber}
        ${a.FloorAndDoor ? `<br><strong>Emelet/ajtó: </strong>${a.FloorAndDoor}` : ""}
        ${a.DoorBell ? `<br><strong>Kapucsengő: </strong>${a.DoorBell}` : ""}
    `);
}