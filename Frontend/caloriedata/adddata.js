document.getElementById("adatmegad").addEventListener("click",(e)=>
{
    var inputAge= document.getElementById("age").value;
    var inputGender = document.getElementById('gender-input');
    let selectedGender = inputGender.options[inputGender.selectedIndex].value;
    var gender;
    
    if(selectedGender == 1){
        gender = 0.9;
    }
    else if(selectedGender == 2){
        gender = 1;
    }

    var inputHeight = document.getElementById("height").value;
    var inputWeight = document.getElementById("weight").value;
    var inputDreamWeight = document.getElementById("dreamweight").value;
    var inputActivity = document.getElementById('activity-input');
    let selectedActivity = inputActivity.options[inputActivity.selectedIndex].value;
    var activity;
    if(selectedActivity == 1)
    {
        activity = 1.2;
    }
    else if (selectedActivity == 2){
        activity = 1.3;
    }
    else if (selectedActivity == 3){
        activity = 1.4;
    }
    else if (selectedActivity == 4){
        activity = 1.5;
    }
    else if (selectedActivity == 5){
        activity = 1.6;
    }
    var inputGoal = document.getElementById('goal-input');
    let selectedGoal = inputGoal.options[inputGoal.selectedIndex].value;
    var goal;
    if(selectedGoal == 1){
        goal = -500;
    }
    else if (selectedGoal == 2){
        goal = 0;
    }
    else if (selectedGoal == 3){
        goal = 500;
    }

    

    var csicska = "https://localhost:44319/api/Calorie";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", csicska, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin","*");



    xhr.setRequestHeader("Content-Type","application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var json = JSON.parse(xhr.responseText);
                console.log(json);
                window.location.href = '/Fitt/fitt.html';
            }
            else if (xhr.status == 400 || xhr.status == 500) {
                let errorMsg = xhr.responseText;
                document.getElementById("backend-error").innerHTML = errorMsg.substring(1, errorMsg.length-1);
                document.getElementById("backend-error").style.display = "block";
            }
        }
    };
    if (getCookie("guid").length != 0) {
        const url = "https://localhost:44319/api/User";
        var rqs = new XMLHttpRequest();
        rqs.open("GET", url, true);
        rqs.setRequestHeader("Access-Control-Allow-Origin", "*");
        rqs.setRequestHeader("Content-Type", "application/json");
        rqs.setRequestHeader('guid', getCookie("guid"));
        rqs.onreadystatechange = function () {
          if (rqs.readyState === 4) {
            if (rqs.status === 200) {
              var myObj = JSON.parse(rqs.responseText);
              console.log(myObj);
              var data = JSON.stringify({
            
                "Name" : myObj.Name,
                "Gender" : `${gender}`,
                "Age" : `${inputAge}`,
                "Height" : `${inputHeight}`,
                "Weight" : `${inputWeight}`,
                "DreamWeight" : `${inputDreamWeight}`,
                "Activity" : `${activity}`,
                "Goal" : `${goal}`
        });
        xhr.send(data);
      
            } else if (rqs.status == 400 || rqs.status == 500) {
              console.log(rqs.responseText);
            }
          }
        };
        var data = JSON.stringify({});
        rqs.send(data);
      }
      
      function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
      }
      
   
    }
)

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}